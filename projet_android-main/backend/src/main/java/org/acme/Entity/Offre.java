package org.acme.Entity;

import java.sql.Timestamp;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class Offre  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    private String titreOffre;
    private String Ville;
    private int CodePostal;
    private LocalDate dateOffre;

    private int Salaire;

    private int numéro;
    private String rue;

    private String typeContrat;
    private String description;
    private Timestamp datePublication;

    public Offre(){}


    public Offre(String titreOffre,String Ville,int CodePostal,LocalDate dateOffre,int Salaire,int numéro,String rue,String typeContrat,String description,
                 Timestamp datePublication) {
        this.titreOffre = titreOffre;
        this.Ville = Ville;
        this.CodePostal = CodePostal;
        this.dateOffre = dateOffre;
        this.Salaire = Salaire;
        this.numéro = numéro;
        this.rue = rue;
        this.typeContrat = typeContrat;
        this.description = description;
        this.datePublication = datePublication;
    }


    public Long getId(){return id;}
    public void setId(Long id){this.id=id;}

    public Timestamp getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Timestamp datePublication) {
        this.datePublication = datePublication;
    }
    public LocalDate getDateOffre() {
        return dateOffre;
    }

    public void setDateOffre(LocalDate dateOffre) {
        this.dateOffre = dateOffre;
    }

    public String getTitreOffre() {
        return titreOffre;
    }

    public void setTitreOffre(String titreOffre) {
        this.titreOffre = titreOffre;
    }

    public String getVille() {
        return Ville;
    }

    public void setVille(String Ville) {
        this.Ville = Ville;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String Rue) {
        this.rue = Rue;
    }

    public int getSalaire() {
        return Salaire;
    }

    public void setSalaire(int Salaire) {
        this.Salaire = Salaire;
    }

    public int getNumero() {
        return numéro;
    }

    public void setNumero(int numero) {
        this.numéro = numero;
    }

    public String getTypeContrat() {
        return typeContrat;
    }

    public void setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCodePostal() {
        return CodePostal;
    }

    public void setCodePostal(int CodePostal) {
        this.CodePostal = CodePostal;
    }
}