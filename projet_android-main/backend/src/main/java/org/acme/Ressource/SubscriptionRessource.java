package org.acme.Ressource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import org.acme.Entity.Subscription;
import org.acme.Service.SubscriptionService;

@Path("/subscriptions")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SubscriptionRessource {

    @Inject
    SubscriptionService subscriptionService;

    @POST
    public Response createSubscription(Subscription subscription) {
        Subscription createdSubscription = subscriptionService.createSubscription(subscription);
        return Response.status(Response.Status.CREATED).entity(createdSubscription).build();
    }

    @GET
    public List<Subscription> getAllSubscriptions() {
        return subscriptionService.getAllSubscriptions();
    }

    @GET
    @Path("/{id}")
    public Response getSubscriptionById(@PathParam("id") Long id) {
        Subscription subscription = subscriptionService.getSubscriptionById(id);
        if (subscription != null) {
            return Response.ok(subscription).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @PUT
    @Path("/{id}")
    public Response updateSubscription(@PathParam("id") Long id, Subscription subscription) {
        subscriptionService.updateSubscription(id, subscription);
        return Response.noContent().build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteSubscription(@PathParam("id") Long id) {
        subscriptionService.deleteSubscription(id);
        return Response.noContent().build();
    }
}