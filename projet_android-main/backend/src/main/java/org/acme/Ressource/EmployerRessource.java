package org.acme.Ressource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import org.acme.Entity.Employer;
import org.acme.Service.EmployerService;

@Path("/Employer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployerRessource {

    @Inject
    EmployerService companyService;

    
    @GET
    public Response getAllCompanies() {
        // Access request URI and print it
       


        List<Employer> companies = companyService.getAllCompanies();
        return Response.ok(companies).build();
    }

    @GET
    @Path("/{id}")
  
    public Response getCompanyById(@PathParam("id") Long id) {
        Employer company = companyService.getCompanyById(id);
        if (company != null) {
            return Response.ok(company).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    public Response createCompany(Employer company) {
        Employer newCompany = companyService.createCompany(company);
        return Response.ok(newCompany).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateCompany(@PathParam("id") String id, Employer company) {
        System.out.println("updateCompany ");
        Employer updatedCompany = companyService.updateCompany(id, company);
        return Response.ok(updatedCompany).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteCompany(@PathParam("id") Long id) {
        companyService.deleteCompany(id);
        return Response.ok().build();
    }

    
}