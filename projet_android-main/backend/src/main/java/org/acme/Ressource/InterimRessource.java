package org.acme.Ressource;


import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.acme.Entity.Interim;
import org.acme.Service.*;

import java.util.List;

@Path("/InterimAgency")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class InterimRessource {

    @Inject
    InterimService agencyService;


    @GET
   
    public Response getAllAgencies() {
        List<Interim> companies = agencyService.getAllAgencies();
        return Response.ok(companies).build();
    }

    @GET
   
    @Path("/{id}")
    public Response getAgencyById(@PathParam("id") Long id) {
       Interim agency =agencyService.getAgencyById(id);
        if (agency != null) {
            return Response.ok(agency).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
   
    public Response createAgency(Interim agency) {
       Interim newAgency = agencyService.createAgency(agency);
        return Response.ok(newAgency).build();
    }

    @PUT
    
    @Path("/{id}")
    public Response updateAgency(@PathParam("id") Long id,Interim agency) {
        System.out.println("updateAgency"  );
        System.out.println("id  = " +id );
       Interim updatedAgency = agencyService.updateAgency(id, agency);
        return Response.ok(updatedAgency).build();
    }

    @DELETE
   
    @Path("/{id}")
    public Response deleteAgency(@PathParam("id") String id) {
        agencyService.deleteAgency(id);
        return Response.ok().build();
    }

    
    
}