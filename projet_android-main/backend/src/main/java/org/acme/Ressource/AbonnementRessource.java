package org.acme.Ressource;



import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.acme.Entity.Abonnement;
import org.acme.Service.AbonnementService;
import javax.ws.rs.core.MediaType;
import com.oracle.svm.core.annotate.Inject;

@Path("/company-subscriptions")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AbonnementRessource {

    @Inject
    AbonnementService companySubscriptionService;

    @GET
    public List<Abonnement> getAll() {
        return companySubscriptionService.findAll();
    }

    @GET
    @Path("/{id}")
    public Abonnement getById(@PathParam("id") Long id) {
        return companySubscriptionService.findById(id);
    }

    @POST
    public Response addCompanySubscription(Abonnement companySubscription) {
        companySubscriptionService.create(companySubscription);
        return Response.ok(companySubscription).status(Response.Status.CREATED).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateCompanySubscription(@PathParam("id") Long id, Abonnement companySubscription) {
        companySubscriptionService.update(id, companySubscription);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteCompanySubscription(@PathParam("id") Long id) {
        companySubscriptionService.delete(id);
        return Response.noContent().build();
    }
}