package org.acme.Ressource;
import javax.inject.Inject;
import javax.transaction.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response;

import org.acme.Entity.Candidat;
import org.acme.Service.CandidatService;


import java.net.URI;
import java.util.List;
import java.util.Optional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/candidates")
public class CandidatRessource {

    @Inject
    CandidatService candidateService;


    @GET
    
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response findAllCandidates() {
        List<Candidat> candidates = candidateService.getAllCandidates();
        if (candidates == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(candidates).build();
    }

    @GET
    @Path("/{id}")
    
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCandidateById(@PathParam("id") Long id) {
        Candidat candidate = candidateService.getCandidateById(id);
        if (candidate != null) {
            return Response.ok(candidate).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @POST
    //@RolesAllowed("admin")
    @Transactional
   
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCandidate( Candidat candidate) {
        candidate = candidateService.create(candidate);
        return Response.ok(candidate).build();
    }

    @PUT
    @Path("/{id}")
    
    public Response updateCandidate(@PathParam("id") Long id, Candidat candidate) {
        candidateService.updateCandidate(id, candidate);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
   
    public Response deleteCandidate(@PathParam("id") Long id) {
        candidateService.deleteCandidate(id);
        return Response.ok().build();
    }

   

}


