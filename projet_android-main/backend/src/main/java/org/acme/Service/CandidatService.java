package org.acme.Service;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.acme.Entity.Candidat;
import org.eclipse.microprofile.config.inject.ConfigProperty;


@ApplicationScoped
public class CandidatService {
    @ConfigProperty(name = "quarkus.http.body.uploads-directory")
    String directory;


    @Inject
    EntityManager em;


    @Transactional
    public Candidat create(Candidat candidate) {
        em.persist(candidate);
        return candidate;
    }

    public List<Candidat> getAllCandidates() {
        return Candidat.listAll();
    }

    public Candidat getCandidateById(Long id) {
        return Candidat.findById(id);
    }


    @Transactional
    public void updateCandidate(Long id, Candidat candidate) {
        Candidat entity = Candidat.findById(id);
        entity.setFirstName(candidate.getFirstName());
        entity.setLastName(candidate.getLastName());
        entity.setEmail(candidate.getEmail());
        entity.persist();
    }

    @Transactional
    public void deleteCandidate(Long id) {
        Candidat.deleteById(id);
    }

    public Candidat findByUserId(Long userId) {
        return Candidat.find("userId", userId).firstResult();
    }


}

