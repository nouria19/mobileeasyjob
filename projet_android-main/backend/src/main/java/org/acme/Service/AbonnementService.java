package org.acme.Service;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.acme.Entity.Abonnement;




@ApplicationScoped
public class AbonnementService {

    public Abonnement findById(Long id) {
        return Abonnement.findById(id);
    }

    public List<Abonnement> findAll() {
        return Abonnement.listAll();
    }

    public void create(Abonnement companySubscription) {
        companySubscription.persist();
    }

    public void update(Long id, Abonnement companySubscription) {
        Abonnement entity = Abonnement.findById(id);
        if (entity != null) {
            entity.setSubscription(companySubscription.getSubscription());
            entity.setCompanyId(companySubscription.getCompanyId());
            entity.setBeginDate(companySubscription.getBeginDate());
            entity.setEndDate(companySubscription.getEndDate());
            entity.setStatus(companySubscription.getStatus());
        }
    }

    public void delete(Long id) {
        Abonnement.deleteById(id);
    }
}