package org.acme.Service;


import javax.transaction.Transactional;

import org.acme.Entity.Interim;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

@ApplicationScoped
public class InterimService {

  

    @Transactional
    public  Interim createAgency(Interim agency) {
        agency.persist();
        return agency;
    }

    public List<Interim> getAllAgencies() {
        return Interim.listAll();
    }

    public Interim getAgencyById(Long id) {

        return Interim.findById(id);
    }


    @Transactional
    public Interim updateAgency(Long id, Interim agency) {
        System.out.println("updateAgency services"  );
        Interim entity = findByUserId(id+"");
        System.out.println("entity.getId()  = " +entity.getId() );
        entity.setCompanyName(agency.getCompanyName());
        entity.setNationalIdNumber(agency.getNationalIdNumber());
        entity.setEmailContact1(agency.getEmailContact1());
        entity.setPhoneNumContact1(agency.getPhoneNumContact1());
        entity.setEmailContact2(agency.getEmailContact2());
        entity.setPhoneNumContact2(agency.getPhoneNumContact2());
        entity.setAddress(agency.getAddress());
        entity.setComplement(agency.getComplement());
        entity.setCity(agency.getCity());
        entity.setCityCode(agency.getCityCode());
        entity.setCountryName(agency.getCountryName());
        entity.setLinkToFacebook(agency.getLinkToFacebook());
        entity.setLinkToTwitter(agency.getLinkToTwitter());
        entity.setLinkToLinkedIn(agency.getLinkToLinkedIn());
        entity.setDescription(agency.getDescription());
        entity.persist();
        return entity;
    }

    @Transactional
    public void deleteAgency(String id) {
       Interim.deleteById(id);
    }
    @Transactional
    public Interim findByUserId(String userId) {
        return Interim.find("userId", userId).firstResult();
    }

    
    

}