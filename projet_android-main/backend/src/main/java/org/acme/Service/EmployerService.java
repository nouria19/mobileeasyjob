package org.acme.Service;

import javax.transaction.Transactional;

import org.acme.Entity.Employer;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;

@ApplicationScoped
public class EmployerService  {
    
    @Transactional
    public Employer createCompany(Employer company) {
        company.persist();
        System.out.println("------------------------------------------------------------------- " );
        System.out.println("company.getId() = " + company.getId());
        System.out.println("company.getUserId() = " + company.getUserId());
        System.out.println("------------------------------------------------------------------- " );
        return company;
    }

    public List<Employer> getAllCompanies() {
        return Employer.listAll();
    }

    public Employer getCompanyById(Long id) {
        return Employer.findById(id);
    }


    @Transactional
    public Employer updateCompany(String id, Employer company) {
        System.out.println("updateCompany id = " +id + " company desc = " + company.getDescription());
        Employer entity = findByUserId(id);
        entity.setCompanyName(company.getCompanyName());
        entity.setDepartementName(company.getDepartementName());
        entity.setSubDepartementName(company.getSubDepartementName());
        entity.setSubDepartementName(company.getSubDepartementName());
        entity.setNationalIdNumber(company.getNationalIdNumber());
        entity.setEmailContact1(company.getEmailContact1());
        entity.setPhoneNumContact1(company.getPhoneNumContact1());
        entity.setEmailContact2(company.getEmailContact2());
        entity.setPhoneNumContact2(company.getPhoneNumContact2());
        entity.setAddress(company.getAddress());
        entity.setComplement(company.getComplement());
        entity.setCity(company.getCity());
        entity.setCityCode(company.getCityCode());
        entity.setCountryName(company.getCountryName());
        entity.setLinkToFacebook(company.getLinkToFacebook());
        entity.setLinkToTwitter(company.getLinkToTwitter());
        entity.setLinkToLinkedIn(company.getLinkToLinkedIn());
        entity.setDescription(company.getDescription());
        entity.persist();
        return entity;
    }

    @Transactional
    public void deleteCompany(Long id) {
        Employer.deleteById(id);
    }
    @Transactional
    public Employer findByUserId(String userId) {
        return Employer.find("userId", userId).firstResult();
    }
   


}