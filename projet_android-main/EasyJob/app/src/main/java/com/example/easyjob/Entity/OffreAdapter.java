package com.example.easyjob.Entity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.easyjob.ModelsAnonyme.OffreDetailsActivity;
import com.example.easyjob.R;

import java.text.SimpleDateFormat;
import java.util.List;

public class OffreAdapter extends RecyclerView.Adapter<OffreAdapter.OffreViewHolder> {

    private List<Offre> offres;
    Context context;
    public OffreAdapter(List<Offre> offres, Context context) {
        this.offres = offres;
        this.context = context;
    }
    public void updateData(List<Offre> newDataList) {
        // Effacement des anciennes données
        offres.clear();

        // Ajout des nouvelles données à la fin de la liste
        offres.addAll(newDataList);

        // Mise à jour de l'interface utilisateur
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OffreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_offre_item, parent, false);
        return new OffreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OffreViewHolder holder, int position) {
        Offre offre = offres.get(position);
        holder.titreOffreTextView.setText(offre.getTitreOffre());
        holder.typeContratTextView.setText(offre.getTypeContrat());
        holder.villeTextView.setText(offre.getVille());
        holder.Salaire_text_view.setText(String.valueOf(offre.getSalaire()));
        holder.rue_text_view.setText(offre.getRue());
        holder.codePTextView.setText(String.valueOf(offre.getCodePostal()));
        holder.numero_text_view.setText(String.valueOf(offre.getNumero()));
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String dateOffreString = dateFormat.format(offre.getDateOffre());
        holder.dateOffreTextView.setText(dateOffreString);
        // Display the datePublication attribute
        String datePublicationString = dateFormat.format(offre.getDatePublication());
        holder.date_publication.setText(datePublicationString);


        holder.itemView.setOnClickListener((v)->{
            Intent intent = new Intent(context, OffreDetailsActivity.class);
            intent.putExtra("titreOffre", offres.get(position).getTitreOffre());
           intent.putExtra("numéro", offres.get(position).getNumero());
            intent.putExtra("rue", offres.get(position).getRue());
            intent.putExtra("Ville", offres.get(position).getVille());
            intent.putExtra("code postal", offres.get(position).getCodePostal());
            intent.putExtra("Salaire", offres.get(position).getSalaire());
            intent.putExtra("Type contrat", offres.get(position).getTypeContrat());
            intent.putExtra("description", offres.get(position).getDescription());
            intent.putExtra("datePublication", offres.get(position).getDatePublication());
            intent.putExtra("dateOffre",offres.get(position).getDateOffre());
            context.startActivity(intent);
        });




    }

    @Override
    public int getItemCount() {
        return offres.size();
    }

    public static class OffreViewHolder extends RecyclerView.ViewHolder {

        public TextView titreOffreTextView;
        public TextView typeContratTextView;
        public TextView villeTextView;
        public TextView codePTextView;
        public TextView dateOffreTextView;
        public TextView Salaire_text_view;

        public TextView numero_text_view;

        public TextView rue_text_view;

        public TextView date_publication;



        public OffreViewHolder(@NonNull View itemView) {
            super(itemView);
            titreOffreTextView = itemView.findViewById(R.id.title_text_view);
            villeTextView = itemView.findViewById(R.id.ville_text_view);
            codePTextView = itemView.findViewById(R.id.codeP_text_view);
            typeContratTextView = itemView.findViewById(R.id.type_text_view);
            Salaire_text_view = itemView.findViewById(R.id.Salaire_text_view);
            dateOffreTextView = itemView.findViewById(R.id.date_depot);
           numero_text_view = itemView.findViewById((R.id.numero_text_view));
            rue_text_view = itemView.findViewById(R.id.rue_text_view);
            date_publication = itemView.findViewById(R.id.date_publication);
        }
    }
    public void setOffres(List<Offre> offres) {
        this.offres = offres;
    }

}
