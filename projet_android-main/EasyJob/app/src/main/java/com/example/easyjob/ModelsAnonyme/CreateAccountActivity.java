package com.example.easyjob.ModelsAnonyme;

import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.easyjob.Entity.DatabaseHelper;

import com.example.easyjob.Entity.UserRole;
import com.example.easyjob.ModelsEmployeur.SelectAbonnActivity;
import com.example.easyjob.ModelsEmployeur.SubscribeDetailActivity;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CreateAccountActivity extends AppCompatActivity {

    EditText emailEditText,passwordEditText,confirmPasswordEditText;
    Button createAccountBtn;
    Button next;
    ProgressBar progressBar;
    TextView loginBtnTextView;
    Spinner choiceSpinner;
    ImageView passwordIcon;
    ImageView conpasswordIcon;
    private boolean passwordshowing = false;
    private boolean conpasswordshowing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        emailEditText = findViewById(R.id.email_edit_text);
        passwordEditText = findViewById(R.id.password_edit_text);
        confirmPasswordEditText = findViewById(R.id.confirm_password_edit_text);
        createAccountBtn = findViewById(R.id.create_account_btn);
        next = findViewById(R.id.next);
        progressBar = findViewById(R.id.progress_bar);
        loginBtnTextView = findViewById(R.id.login_text_view_btn);
        passwordIcon = findViewById(R.id.passwordIcon);
        conpasswordIcon = findViewById(R.id.conpasswordIcon);
        passwordIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(passwordshowing){
                    passwordshowing=false;
                    passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordIcon.setImageResource(R.drawable.password_show);

                }else {
                    passwordshowing=true;

                    passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    passwordIcon.setImageResource(R.drawable.password_hide);
                }
                passwordEditText.setSelection(passwordEditText.length());
            }
        });


        conpasswordIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(conpasswordshowing){
                    conpasswordshowing=false;
                    confirmPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    conpasswordIcon.setImageResource(R.drawable.password_show);

                }else {
                    conpasswordshowing=true;

                    confirmPasswordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    conpasswordIcon.setImageResource(R.drawable.password_hide);
                }
                confirmPasswordEditText.setSelection(confirmPasswordEditText.length());
            }
        });

        //createAccountBtn.setOnClickListener(v-> createAccount());
        loginBtnTextView.setOnClickListener(v-> finish());

        choiceSpinner = findViewById(R.id.choice_spinner);
        choiceSpinner.setSelection(-1);

        choiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Récupérer le choix sélectionné
                String selectedChoice = parent.getItemAtPosition(position).toString();
                // Afficher ou masquer les autres éléments en fonction du choix sélectionné
                if (selectedChoice.equals("Chercheur d'emploi")) {
                    // Afficher les éléments pour le choix 1
                    findViewById(R.id.editText1).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText2).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText3).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText7).setVisibility(View.VISIBLE);

                    findViewById(R.id.email_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.password_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.confirm_password_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.create_account_btn).setVisibility(View.VISIBLE);
                    findViewById(R.id.create).setVisibility(View.VISIBLE);

                    findViewById(R.id.editText4).setVisibility(View.GONE);
                    findViewById(R.id.editText5).setVisibility(View.GONE);
                    findViewById(R.id.editText6).setVisibility(View.GONE);
                    findViewById(R.id.editText8).setVisibility(View.GONE);
                    findViewById(R.id.editText9).setVisibility(View.GONE);
                    findViewById(R.id.editText10).setVisibility(View.GONE);
                    findViewById(R.id.editText11).setVisibility(View.GONE);
                    findViewById(R.id.editText12).setVisibility(View.GONE);


                    createAccountBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // Récupérer les valeurs des champs d'inscription
                            String email = ((EditText) findViewById(R.id.email_edit_text)).getText().toString();
                            String password = ((EditText) findViewById(R.id.password_edit_text)).getText().toString();
                            String nom = ((EditText) findViewById(R.id.editText1)).getText().toString();
                            String prenom = ((EditText) findViewById(R.id.editText2)).getText().toString();
                            String dateNaissString = ((EditText) findViewById(R.id.editText3)).getText().toString();
                            String nationalite = ((EditText) findViewById(R.id.editText7)).getText().toString();
                            String confirmPassword = ((EditText) findViewById(R.id.confirm_password_edit_text)).getText().toString();



                            // Vérifier que tous les champs sont remplis
                            if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ||
                                    nom.isEmpty() || prenom.isEmpty() || dateNaissString.isEmpty()) {
                                Toast.makeText(CreateAccountActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Appeler la méthode addUser() avec les valeurs obtenues
                            DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());

                                // Vérifier si l'adresse e-mail est déjà utilisée par un autre utilisateur
                            if (dbHelper.checkIfEmailExists(email)) {
                                Toast.makeText(CreateAccountActivity.this, "Adresse e-mail déjà utilisée", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier la validité de l'adresse e-mail
                            if (!isValidEmail(email)) {
                                Toast.makeText(CreateAccountActivity.this, "Adresse e-mail invalide", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier la validité du mot de passe
                            if (!isValidPassword(password)) {
                                Toast.makeText(CreateAccountActivity.this, "Mot de passe doit contenir au moins une Majuscule, un chiffres et un caractère spécial", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier que le mot de passe saisi est égal à celui saisi dans la confirmation du mot de passe
                            if (!password.equals(confirmPassword)) {
                                Toast.makeText(CreateAccountActivity.this, "Les mots de passe ne correspondent pas", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier la validité de la date de naissance
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                            Date dateNaiss = null;
                            try {
                                dateNaiss = sdf.parse(dateNaissString);
                                // Vérifier si la date parsée est égale à la date saisie, cela signifie que la date est valide
                                if (!sdf.format(dateNaiss).equals(dateNaissString)) {
                                    Toast.makeText(CreateAccountActivity.this, "Format de date de naissance invalide", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            } catch (ParseException e) {
                                Toast.makeText(CreateAccountActivity.this, "Format de date de naissance invalide", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            Intent intent=new Intent(CreateAccountActivity.this,OTPVerification.class);

                            intent.putExtra("Email",email);
                            intent.putExtra("password",password);
                            intent.putExtra("nom",nom);
                            intent.putExtra("prenom",prenom);
                            intent.putExtra("dateNaissString",dateNaissString);
                            intent.putExtra("nationalite",nationalite);
                            intent.putExtra("Role", UserRole.CANDIDAT.getValue());
                            startActivity(intent);

                        }
                    });


                } else if (selectedChoice.equals("Entreprise")) {

                    findViewById(R.id.editText4).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText5).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText6).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText8).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText9).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText10).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText11).setVisibility(View.VISIBLE);

                    findViewById(R.id.email_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.password_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.confirm_password_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.next).setVisibility(View.VISIBLE);
                    findViewById(R.id.create).setVisibility(View.VISIBLE);

                    findViewById(R.id.editText12).setVisibility(View.GONE);
                    findViewById(R.id.create_account_btn).setVisibility(View.GONE);
                    findViewById(R.id.editText1).setVisibility(View.GONE);
                    findViewById(R.id.editText2).setVisibility(View.GONE);
                    findViewById(R.id.editText3).setVisibility(View.GONE);

                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // Récupérer les valeurs des champs d'inscription

                            String Company_Name = ((EditText) findViewById(R.id.editText4)).getText().toString();
                            String Service_Name =  ((EditText) findViewById(R.id.editText5)).getText().toString();
                            String Under_Service = ((EditText) findViewById(R.id.editText6)).getText().toString();
                            String National_Number= ((EditText) findViewById(R.id.editText8)).getText().toString();
                            String Contact1= ((EditText) findViewById(R.id.editText9)).getText().toString();
                            String Contact2= ((EditText) findViewById(R.id.editText10)).getText().toString();
                            String Phone_Number= ((EditText) findViewById(R.id.editText11)).getText().toString();

                            String email = ((EditText) findViewById(R.id.email_edit_text)).getText().toString();
                            String password = ((EditText) findViewById(R.id.password_edit_text)).getText().toString();
                            String confirmPassword = ((EditText)findViewById(R.id.confirm_password_edit_text)).getText().toString();


                            // Vérifier que tous les champs sont remplis
                            if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ||
                                    Company_Name.isEmpty() || Service_Name.isEmpty() || National_Number.isEmpty()
                            || Phone_Number.isEmpty()) {
                                Toast.makeText(CreateAccountActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Appeler la méthode addUser() avec les valeurs obtenues
                            DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());

                            // Vérifier si l'adresse e-mail est déjà utilisée par un autre utilisateur
                            if (dbHelper.checkIfEmailExistsCompany(email)) {
                                Toast.makeText(CreateAccountActivity.this, "Adresse e-mail déjà utilisée", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier la validité de l'adresse e-mail
                            if (!isValidEmail(email)) {
                                Toast.makeText(CreateAccountActivity.this, "Adresse e-mail invalide", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier la validité du mot de passe
                            if (!isValidPassword(password)) {
                                Toast.makeText(CreateAccountActivity.this, "Mot de passe doit contenir au moins une Majuscule, un chiffres et un caractère spécial", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier que le mot de passe saisi est égal à celui saisi dans la confirmation du mot de passe
                            if (!password.equals(confirmPassword)) {
                                Toast.makeText(CreateAccountActivity.this, "Les mots de passe ne correspondent pas", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            int phoneNumberInt = Integer.parseInt(Phone_Number);

                            Intent intent=new Intent(CreateAccountActivity.this, SelectAbonnActivity.class);
                            intent.putExtra("Email",email);
                            intent.putExtra("password",password);
                            intent.putExtra("name",Company_Name);
                            intent.putExtra("departementName",Service_Name);
                            intent.putExtra("subDepartementName",Under_Service);
                            intent.putExtra("companyNationalId",National_Number);
                            intent.putExtra("contact1Name",Contact1);
                            intent.putExtra("contact2Name",Contact2);
                            intent.putExtra("companyNationalId",National_Number);
                            intent.putExtra("numero",phoneNumberInt);
                            intent.putExtra("Role", UserRole.ENTREPRISE.getValue());
                            startActivity(intent);

                        }



                    });

                } else if (selectedChoice.equals("Agence d'intérim")) {

                    // Afficher les éléments pour le choix 3
                    findViewById(R.id.editText12).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText5).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText6).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText8).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText9).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText10).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText11).setVisibility(View.VISIBLE);

                    findViewById(R.id.email_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.password_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.confirm_password_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.next).setVisibility(View.VISIBLE);
                    findViewById(R.id.create).setVisibility(View.VISIBLE);

                    findViewById(R.id.editText1).setVisibility(View.GONE);
                    findViewById(R.id.editText2).setVisibility(View.GONE);
                    findViewById(R.id.editText3).setVisibility(View.GONE);
                    findViewById(R.id.editText4).setVisibility(View.GONE);
                    findViewById(R.id.create_account_btn).setVisibility(View.GONE);

                    next.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // Récupérer les valeurs des champs d'inscription

                            String Agence_Name = ((EditText) findViewById(R.id.editText12)).getText().toString();
                            String Service_Name = ((EditText) findViewById(R.id.editText5)).getText().toString();
                            String Under_Service = ((EditText) findViewById(R.id.editText6)).getText().toString();
                            String National_Number = ((EditText) findViewById(R.id.editText8)).getText().toString();
                            String Contact1 = ((EditText) findViewById(R.id.editText9)).getText().toString();
                            String Contact2 = ((EditText) findViewById(R.id.editText10)).getText().toString();
                            String Phone_Number = ((EditText) findViewById(R.id.editText11)).getText().toString();

                            String email = ((EditText) findViewById(R.id.email_edit_text)).getText().toString();
                            String password = ((EditText) findViewById(R.id.password_edit_text)).getText().toString();
                            String confirmPassword = ((EditText) findViewById(R.id.confirm_password_edit_text)).getText().toString();


                            // Vérifier que tous les champs sont remplis
                            if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() ||
                                    Agence_Name.isEmpty() || Service_Name.isEmpty() || National_Number.isEmpty()
                                    || Phone_Number.isEmpty()) {
                                Toast.makeText(CreateAccountActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Appeler la méthode addUser() avec les valeurs obtenues
                            DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());

                            // Vérifier si l'adresse e-mail est déjà utilisée par un autre utilisateur
                            if (dbHelper.checkIfEmailExists(email)) {
                                Toast.makeText(CreateAccountActivity.this, "Adresse e-mail déjà utilisée", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier la validité de l'adresse e-mail
                            if (!isValidEmail(email)) {
                                Toast.makeText(CreateAccountActivity.this, "Adresse e-mail invalide", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier la validité du mot de passe
                            if (!isValidPassword(password)) {
                                Toast.makeText(CreateAccountActivity.this, "Mot de passe doit contenir au moins une Majuscule, un chiffres et un caractère spécial", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            // Vérifier que le mot de passe saisi est égal à celui saisi dans la confirmation du mot de passe
                            if (!password.equals(confirmPassword)) {
                                Toast.makeText(CreateAccountActivity.this, "Les mots de passe ne correspondent pas", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            int phoneNumberInt = Integer.parseInt(Phone_Number);

                            Intent intent = new Intent(CreateAccountActivity.this, SelectAbonnActivity.class);
                            intent.putExtra("Email", email);
                            intent.putExtra("password", password);
                            intent.putExtra("Agencename", Agence_Name);
                            intent.putExtra("departementName", Service_Name);
                            intent.putExtra("subDepartementName", Under_Service);
                            intent.putExtra("companyNationalId", National_Number);
                            intent.putExtra("contact1Name", Contact1);
                            intent.putExtra("contact2Name", Contact2);
                            intent.putExtra("companyNationalId", National_Number);
                            intent.putExtra("numero", phoneNumberInt);
                            intent.putExtra("Role", UserRole.AGENCE.getValue());
                            startActivity(intent);

                        }

                    });

                }else if (selectedChoice.equals("Gestionnaire")) {
                    findViewById(R.id.editText1).setVisibility(View.VISIBLE);
                    findViewById(R.id.editText2).setVisibility(View.VISIBLE);
                    findViewById(R.id.email_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.password_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.confirm_password_edit_text).setVisibility(View.VISIBLE);
                    findViewById(R.id.create_account_btn).setVisibility(View.VISIBLE);
                    findViewById(R.id.create).setVisibility(View.VISIBLE);

                    findViewById(R.id.editText3).setVisibility(View.GONE);
                    findViewById(R.id.editText4).setVisibility(View.GONE);
                    findViewById(R.id.editText5).setVisibility(View.GONE);
                    findViewById(R.id.editText6).setVisibility(View.GONE);
                    findViewById(R.id.editText8).setVisibility(View.GONE);
                    findViewById(R.id.editText9).setVisibility(View.GONE);
                    findViewById(R.id.editText10).setVisibility(View.GONE);
                    findViewById(R.id.editText11).setVisibility(View.GONE);
                    findViewById(R.id.editText12).setVisibility(View.GONE);

                    createAccountBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Ne rien faire si aucun choix n'est sélectionné
                choiceSpinner.setEnabled(false);
            }
        });

    }
    // Vérifier la validité de l'adresse e-mail
    private boolean isValidEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return email.matches(emailPattern);
    }

    // Vérifier la validité du mot de passe
    private boolean isValidPassword(String password) {
        // Vérifier la présence d'au moins une majuscule, un chiffre et un caractère spécial
        String passwordPattern = "(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]+";
        return password.matches(passwordPattern);
    }




}