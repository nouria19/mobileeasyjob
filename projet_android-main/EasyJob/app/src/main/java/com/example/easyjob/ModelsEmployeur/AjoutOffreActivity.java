package com.example.easyjob.ModelsEmployeur;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.easyjob.Entity.DatabaseHelper;
import com.example.easyjob.Entity.Offre;
import com.example.easyjob.Entity.UserRole;
import com.example.easyjob.ModelsCandidat.AcceuilCActivity;
import com.example.easyjob.ModelsCandidat.ProfilCActivity;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AjoutOffreActivity extends AppCompatActivity {

    private EditText titreOffreEditText;
    private EditText villeEditText;
    private EditText codePostalEditText;
    private EditText dateOffreEditText;
    private EditText salaireEditText;
    private EditText numeroEditText;
    private EditText rueEditText;
    private EditText typeContratEditText;
    private EditText descriptionEditText;

    private Button ajouterOffreButton;
    DatabaseHelper databaseHelper;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_offre);

        // Récupération de l'intent qui a lancé cette activité
        Intent intent = getIntent();
        String getRole = getIntent().getStringExtra("Role");

        // Instancier DatabaseHelper
        databaseHelper = new DatabaseHelper(this);

        sessionManager = SessionManager.getInstance();
        sessionManager.setContext(getApplicationContext());

        titreOffreEditText = findViewById(R.id.titreOffreEditText);
        villeEditText = findViewById(R.id.villeEditText);
        codePostalEditText = findViewById(R.id.codePostalEditText);
        dateOffreEditText = findViewById(R.id.dateOffreEditText);
        salaireEditText = findViewById(R.id.salaireEditText);
        numeroEditText = findViewById(R.id.numeroEditText);
        rueEditText = findViewById(R.id.rueEditText);
        typeContratEditText = findViewById(R.id.typeContratEditText);
        descriptionEditText = findViewById(R.id.descriptionEditText);

        ajouterOffreButton = findViewById(R.id.ajouterOffreButton);
        if(getRole.equals("entreprise")) {
            ajouterOffreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ajouterOffre();
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }else if(getRole.equals("agence")){
            ajouterOffreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        ajouterOffre2();
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }
    }

    private void ajouterOffre() throws JsonProcessingException {
        String titreOffre = titreOffreEditText.getText().toString();
        String ville = villeEditText.getText().toString();
        int codePostal = Integer.parseInt(codePostalEditText.getText().toString());
        String dateOffre = dateOffreEditText.getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(dateOffre);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        int salaire = Integer.parseInt(salaireEditText.getText().toString());
        int numero = Integer.parseInt(numeroEditText.getText().toString());
        String rue = rueEditText.getText().toString();
        String typeContrat = typeContratEditText.getText().toString();
        String description = descriptionEditText.getText().toString();

        // Créer une instance de la classe Offre avec les données saisies
        Offre offre = new Offre();
        offre.setTitreOffre(titreOffre);
        offre.setVille(ville);
        offre.setCodePostal(codePostal);
        offre.setDateOffre(date); // À gérer la conversion de la chaîne en objet Date
        offre.setSalaire(salaire);
        offre.setNumero(numero);
        offre.setRue(rue);
        offre.setTypeContrat(typeContrat);
        offre.setDescription(description);

        String email = sessionManager.getCurrentCompany().getEmail();
        databaseHelper.ajouto(email,offre);



        // Afficher un message de succès
        Toast.makeText(this, "Offre ajoutée avec succès", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(AjoutOffreActivity.this, AcceuilCActivity.class);
        String getRole = UserRole.ENTREPRISE.getValue();
        intent.putExtra("Role", getRole);
        startActivity(intent);
    }

    private void ajouterOffre2() throws JsonProcessingException {
        String titreOffre = titreOffreEditText.getText().toString();
        String ville = villeEditText.getText().toString();
        int codePostal = Integer.parseInt(codePostalEditText.getText().toString());
        String dateOffre = dateOffreEditText.getText().toString();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = null;
        try {
            date = dateFormat.parse(dateOffre);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        int salaire = Integer.parseInt(salaireEditText.getText().toString());
        int numero = Integer.parseInt(numeroEditText.getText().toString());
        String rue = rueEditText.getText().toString();
        String typeContrat = typeContratEditText.getText().toString();
        String description = descriptionEditText.getText().toString();

        // Créer une instance de la classe Offre avec les données saisies
        Offre offre = new Offre();
        offre.setTitreOffre(titreOffre);
        offre.setVille(ville);
        offre.setCodePostal(codePostal);
        offre.setDateOffre(date);
        offre.setSalaire(salaire);
        offre.setNumero(numero);
        offre.setRue(rue);
        offre.setTypeContrat(typeContrat);
        offre.setDescription(description);

        String email = sessionManager.getCurrentAgence().getEmail();
        databaseHelper.ajoutoA(email,offre);



        // Afficher un message de succès
        Toast.makeText(this, "Offre ajoutée avec succès", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(AjoutOffreActivity.this, AcceuilCActivity.class);
        String getRole = UserRole.AGENCE.getValue();
        intent.putExtra("Role", getRole);
        startActivity(intent);
    }
}
