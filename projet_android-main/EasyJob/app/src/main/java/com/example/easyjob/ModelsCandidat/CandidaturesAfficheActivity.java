package com.example.easyjob.ModelsCandidat;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.easyjob.Entity.DatabaseHelper;
import com.example.easyjob.R;

import java.util.ArrayList;
import java.util.List;

public class CandidaturesAfficheActivity extends AppCompatActivity {

    private ListView listViewCandidatures;
    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidatures_affiche);

        listViewCandidatures = findViewById(R.id.listViewCandidatures);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        listViewCandidatures.setAdapter(adapter);

        displayCandidatures();
    }
    @SuppressLint("Range")
    private void displayCandidatures() {
        DatabaseHelper dbHelper = new DatabaseHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query("Candidature", null, null, null, null, null, null);

        List<String> candidaturesList = new ArrayList<>();

        while (cursor.moveToNext()) {
            String nom = cursor.getString(cursor.getColumnIndex("nom"));
            String prenom = cursor.getString(cursor.getColumnIndex("prenom"));
            String dateNaissance = cursor.getString(cursor.getColumnIndex("dateNaissance"));
            String nationalite = cursor.getString(cursor.getColumnIndex("nationalite"));


            // Ajouter les informations des candidatures à la liste
            candidaturesList.add("Nom: " + nom + "\n" +
                    "Prénom: " + prenom + "\n" +
                    "Date de naissance: " + dateNaissance + "\n" +
                    "Nationalité: " + nationalite + "\n"
            );
        }

        cursor.close();
        db.close();

        // Mettre à jour l'adaptateur avec la liste des candidatures
        adapter.addAll(candidaturesList);
        adapter.notifyDataSetChanged();
    }
}

