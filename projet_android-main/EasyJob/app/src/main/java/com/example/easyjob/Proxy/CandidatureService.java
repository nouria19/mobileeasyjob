package com.example.easyjob.Proxy;


import com.example.easyjob.Entity.Candidature;

import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface CandidatureService {
    @POST("offres/{offerId}/candidature")

    Call<Candidature> createCandidature(@Path("offerId") Long offerId, @Body Candidature candidature);

    @GET("offres/{offerId}/candidature")

    Call<List<Candidature>> getCandidaturesByOfferId(@Path("offerId") Long offerId);

    @GET("offres/{offerId}/candidature/{id}")

    Call<Candidature> getCandidatureById(@Path("offerId") Long offerId, @Path("id") Long id);
}
