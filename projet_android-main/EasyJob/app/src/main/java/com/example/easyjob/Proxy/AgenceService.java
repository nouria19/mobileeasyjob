package com.example.easyjob.Proxy;


import com.example.easyjob.Entity.Agence;

import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface AgenceService {
    @GET("InterimAgency")
    Call<List<Agence>> getAllAgencies();

    @GET("InterimAgency/{id}")
    Call<Agence> getAgencyById(@Path("id") Long id);

    @POST("InterimAgency")
    Call<Agence> createAgency(@Body Agence agency);

    @PUT("InterimAgency/{id}")
    Call<Agence> updateAgency(@Path("id") Long id, @Body Agence agency);

    @DELETE("InterimAgency/{id}")
    Call<Void> deleteAgency(@Path("id") String id);
}
