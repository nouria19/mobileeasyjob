package com.example.easyjob.Proxy;

import com.example.easyjob.Entity.Offre;
import com.example.easyjob.Entity.Subscription;
import com.example.easyjob.Proxy.AbonnementService;
import com.example.easyjob.Proxy.OffreService;
import com.example.easyjob.Proxy.AgenceService;
import com.example.easyjob.Entity.Agence;
import com.example.easyjob.Proxy.CandidatService;
import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Proxy.CandidatureService;
import com.example.easyjob.Entity.Candidature;
import com.example.easyjob.Proxy.EmployerService;
import com.example.easyjob.Entity.Company;
import com.google.gson.Gson;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;

public class ApiManager {
    private static final String BASE_URL = "http://192.168.2.201:8081/";

    private Retrofit retrofit;
    private OffreService offreService;
    private AbonnementService abonnementService;
    private AgenceService agenceService;
    private CandidatService candidatService;
    private CandidatureService candidatureService;
    private EmployerService employerService;

    public ApiManager() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.16.201:8081")
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();

        offreService = retrofit.create(OffreService.class);
        abonnementService = retrofit.create(AbonnementService.class);
        agenceService = retrofit.create(AgenceService.class);
        candidatService = retrofit.create(CandidatService.class);
        candidatureService = retrofit.create(CandidatureService.class);
        employerService = retrofit.create(EmployerService.class);
    }

    // Méthodes pour les offres

    public void getAllOffres(Callback<List<Offre>> callback) {
        Call<List<Offre>> call = offreService.getAllOffres();
        call.enqueue(callback);
    }

    public void getOffreById(String id, Callback<Offre> callback) {
        Call<Offre> call = offreService.getOffreById(id);
        call.enqueue(callback);
    }

    public void createOffre(Offre offre, Callback<Void> callback) {
        Call<Void> call = offreService.createOffre(offre);
        call.enqueue(callback);
    }

    public void updateOffre(String id, Offre updatedOffre, Callback<Void> callback) {
        Call<Void> call = offreService.updateOffre(id, updatedOffre);
        call.enqueue(callback);
    }

    public void deleteOffre(String id, Callback<Void> callback) {
        Call<Void> call = offreService.deleteOffre(id);
        call.enqueue(callback);
    }

    // Méthodes pour les abonnements

    public void getAllAbonnements(Callback<List<Subscription>> callback) {
        Call<List<Subscription>> call = abonnementService.getAllAbonnements();
        call.enqueue(callback);
    }

    public void getAbonnementById(Long id, Callback<Subscription> callback) {
        Call<Subscription> call = abonnementService.getAbonnementById(id);
        call.enqueue(callback);
    }

    public void createAbonnement(Subscription abonnement, Callback<Void> callback) {
        Call<Void> call = abonnementService.createAbonnement(abonnement);
        call.enqueue(callback);
    }

    public void updateAbonnement(Long id, Subscription updatedAbonnement, Callback<Void> callback) {
        Call<Void> call = abonnementService.updateAbonnement(id, updatedAbonnement);
        call.enqueue(callback);
    }

    public void deleteAbonnement(Long id, Callback<Void> callback) {
        Call<Void> call = abonnementService.deleteAbonnement(id);
        call.enqueue(callback);
    }

    // Méthodes pour les agences

    public void getAllAgencies(Callback<List<Agence>> callback) {
        Call<List<Agence>> call = agenceService.getAllAgencies();
        call.enqueue(callback);
    }

    public void getAgencyById(String id, Callback<Agence> callback) {
        Call<Agence> call = agenceService.getAgencyById(Long.valueOf(id));
        call.enqueue(callback);
    }

    public void createAgency(Agence agency, Callback<Void> callback) {
        Call<Agence> call = agenceService.createAgency(agency);
        //call.enqueue(callback);
    }

    public void updateAgency(Long id, Agence updatedAgency, Callback<Void> callback) {
        Call<Agence> call = agenceService.updateAgency(id, updatedAgency);
        ///call.enqueue(callback);
    }


    public void deleteAgency(String id, Callback<Void> callback) {
        Call<Void> call = agenceService.deleteAgency(id);
        call.enqueue(callback);
    }

    // Méthodes pour les candidats

    public void getAllCandidates(Callback<List<Candidat>> callback) {
        Call<List<Candidat>> call = candidatService.getAllCandidates();
        call.enqueue(callback);
    }

    public void getCandidateById(Long id, Callback<Candidat> callback) {
        Call<Candidat> call = candidatService.getCandidateById(id);
        call.enqueue(callback);
    }

    public void createCandidate(Candidat candidate, Callback<Candidat> callback) {
        Call<Void> call = candidatService.addCandidate(candidate);
        //call.enqueue(callback);
    }

    public void updateCandidate(Long id, Candidat updatedCandidate, Callback<Candidat> callback) {
        Call<Void> call = candidatService.updateCandidate(id, updatedCandidate);
        //call.enqueue(callback);
    }

    public void deleteCandidate(Long id, Callback<Void> callback) {
        Call<Void> call = candidatService.deleteCandidate(id);
        call.enqueue(callback);
    }

    // Méthodes pour les candidatures

    public void createCandidature(Long offerId, Candidature candidature, Callback<Candidature> callback) {
        Call<Candidature> call = candidatureService.createCandidature(offerId, candidature);
        call.enqueue(callback);
    }

    public void getCandidaturesByOfferId(Long offerId, Callback<List<Candidature>> callback) {
        Call<List<Candidature>> call = candidatureService.getCandidaturesByOfferId(offerId);
        call.enqueue(callback);
    }

    public void getCandidatureById(Long offerId, Long id, Callback<Candidature> callback) {
        Call<Candidature> call = candidatureService.getCandidatureById(offerId, id);
        call.enqueue(callback);
    }

    // Méthodes pour les employeurs

    public void getAllCompanies(Callback<List<Company>> callback) {
        Call<List<Company>> call = employerService.getAllCompanies();
        call.enqueue(callback);
    }

    public void getCompanyById(Long id, Callback<Company> callback) {
        Call<Company> call = employerService.getCompanyById(id);
        call.enqueue(callback);
    }

    public void createCompany(Company company, Callback<Company> callback) {
        Call<Company> call = employerService.createCompany(company);
        call.enqueue(callback);
    }

    public void updateCompany(Long id, Company updatedCompany, Callback<Company> callback) {
        Call<Company> call = employerService.updateCompany(id, updatedCompany);
        call.enqueue(callback);
    }

    public void deleteCompany(Long id, Callback<Void> callback) {
        Call<Void> call = employerService.deleteCompany(id);
        call.enqueue(callback);
    }

    public void setServerIpAddress(String serverIpAddress) {
        String newBaseUrl = "http://" + serverIpAddress + ":8081/";
        retrofit = retrofit.newBuilder()
                .baseUrl(newBaseUrl)
                .build();
        offreService = retrofit.create(OffreService.class);
        abonnementService = retrofit.create(AbonnementService.class);
        agenceService = retrofit.create(AgenceService.class);
        candidatService = retrofit.create(CandidatService.class);
        candidatureService = retrofit.create(CandidatureService.class);
        employerService = retrofit.create(EmployerService.class);
    }
}
