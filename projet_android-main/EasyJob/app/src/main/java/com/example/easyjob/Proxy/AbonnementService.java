package com.example.easyjob.Proxy;

import com.example.easyjob.Entity.Subscription;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AbonnementService {
    @GET("abonnements")
    Call<List<Subscription>> getAllAbonnements();

    @GET("abonnements/{id}")
    Call<Subscription> getAbonnementById(@Path("id") Long id);

    @POST("abonnements")
    Call<Void> createAbonnement(@Body Subscription abonnement);

    @PUT("abonnements/{id}")
    Call<Void> updateAbonnement(@Path("id") Long id, @Body Subscription updatedAbonnement);

    @DELETE("abonnements/{id}")
    Call<Void> deleteAbonnement(@Path("id") Long id);
}