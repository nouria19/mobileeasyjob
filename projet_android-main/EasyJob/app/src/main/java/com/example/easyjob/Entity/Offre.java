package com.example.easyjob.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class Offre implements Parcelable {
    private Long id;
    private String titreOffre;
    private String Ville;
    private int CodePostal;
    private Date dateOffre;

    private int Salaire;

    private int numéro;
    private String rue;

    private String typeContrat;
    private String description;
    private Timestamp datePublication;

    private ArrayList<Candidature> candidatures = new ArrayList<>();

    public Offre(){}


    public Offre(String titreOffre,String Ville,int CodePostal,Date dateOffre,int Salaire,int numéro,String rue,String typeContrat,String description,
                 Timestamp datePublication) {
        this.titreOffre = titreOffre;
        this.Ville = Ville;
        this.CodePostal = CodePostal;
        this.dateOffre = dateOffre;
        this.Salaire = Salaire;
        this.numéro = numéro;
        this.rue = rue;
        this.typeContrat = typeContrat;
        this.description = description;
        this.datePublication = datePublication;
    }


    public Long getId(){return id;}
    public void setId(Long id){this.id=id;}

    public Timestamp getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Timestamp datePublication) {
        this.datePublication = datePublication;
    }
    public Date getDateOffre() {
        return dateOffre;
    }

    public void setDateOffre(Date dateOffre) {
        this.dateOffre = dateOffre;
    }

    public String getTitreOffre() {
        return titreOffre;
    }

    public void setTitreOffre(String titreOffre) {
        this.titreOffre = titreOffre;
    }

    public String getVille() {
        return Ville;
    }

    public void setVille(String Ville) {
        this.Ville = Ville;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String Rue) {
        this.rue = Rue;
    }

    public int getSalaire() {
        return Salaire;
    }

    public void setSalaire(int Salaire) {
        this.Salaire = Salaire;
    }

    public int getNumero() {
        return numéro;
    }

    public void setNumero(int numero) {
        this.numéro = numero;
    }

    public String getTypeContrat() {
        return typeContrat;
    }

    public void setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCodePostal() {
        return CodePostal;
    }

    public void setCodePostal(int CodePostal) {
        this.CodePostal = CodePostal;
    }

    // Implémentation de l'interface Parcelable
    protected Offre(Parcel in) {
        Ville = in.readString();
        CodePostal = Integer.parseInt(in.readString());
        numéro = Integer.parseInt(in.readString());
        titreOffre = in.readString();
        rue = in.readString();
        description = in.readString();
        dateOffre = new Date(in.readLong());
        datePublication = new Timestamp(in.readLong());
        typeContrat = in.readString();
        Salaire = Integer.parseInt(in.readString());
        in.readTypedList(candidatures, Candidature.CREATOR);
    }

    public static final Parcelable.Creator<Offre> CREATOR = new Parcelable.Creator<Offre>() {
        @Override
        public Offre createFromParcel(Parcel in) {
            return new Offre(in);
        }

        @Override
        public Offre[] newArray(int size) {
            return new Offre[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Ville);
        parcel.writeString(String.valueOf(CodePostal));
        parcel.writeString(String.valueOf(Salaire));
        parcel.writeString(String.valueOf(numéro));
        parcel.writeString(titreOffre);
        parcel.writeString(rue);
        parcel.writeString(description);
        parcel.writeLong(dateOffre.getTime());
        parcel.writeString(typeContrat);
        parcel.writeLong(datePublication.getTime());
        parcel.writeTypedList(candidatures);
    }

    public int getNuméro() {
        return numéro;
    }

    public void setNuméro(int numéro) {
        this.numéro = numéro;
    }

    public ArrayList<Candidature> getCandidatures() {
        return candidatures;
    }

    public void setCandidatures(ArrayList<Candidature> candidatures) {
        this.candidatures = candidatures;
    }
}

