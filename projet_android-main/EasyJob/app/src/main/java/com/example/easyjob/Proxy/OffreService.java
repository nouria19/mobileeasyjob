package com.example.easyjob.Proxy;

import com.example.easyjob.Entity.Offre;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.*;

public interface OffreService {
    @GET("offres")
    Call<List<Offre>> getAllOffres();

    @GET("offres/{id}")
    Call<Offre> getOffreById(@Path("id") String id);

    @POST("offres")
    Call<Void> createOffre(@Body Offre offre);

    @PUT("offres/{id}")
    Call<Void> updateOffre(@Path("id") String id, @Body Offre updatedOffre);

    @DELETE("offres/{id}")
    Call<Void> deleteOffre(@Path("id") String id);
}

