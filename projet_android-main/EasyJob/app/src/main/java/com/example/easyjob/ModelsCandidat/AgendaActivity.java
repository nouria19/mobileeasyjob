package com.example.easyjob.ModelsCandidat;

import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.easyjob.R;

public class AgendaActivity extends AppCompatActivity {

    private CalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        calendarView = findViewById(R.id.calendar_view);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                // Traitement de la date sélectionnée
                String selectedDate = dayOfMonth + "/" + (month + 1) + "/" + year;
                Toast.makeText(AgendaActivity.this, "Date sélectionnée : " + selectedDate, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
