package com.example.easyjob.ModelsAnonyme;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.easyjob.Entity.DatabaseHelper;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Utility {
    static void showToast(Context context, String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }

    static SQLiteDatabase getWritableDatabase(Context context) {
        return new DatabaseHelper(context).getWritableDatabase();
    }

    static String timestampToString(long timestamp){
        Date date = new Date(timestamp);
        return new SimpleDateFormat("MM/dd/yyyy").format(date);
    }

}
