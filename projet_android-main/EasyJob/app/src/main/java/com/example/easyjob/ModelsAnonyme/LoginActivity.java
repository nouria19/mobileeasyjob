package com.example.easyjob.ModelsAnonyme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.easyjob.Entity.Agence;
import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Entity.Company;
import com.example.easyjob.Entity.DatabaseHelper;
import com.example.easyjob.Entity.UserRole;
import com.example.easyjob.ModelsCandidat.AcceuilCActivity;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;

import org.mindrot.jbcrypt.BCrypt;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    EditText emailEditText,passwordEditText;
    Button loginBtn;
    ProgressBar progressBar;
    TextView createAccountBtnTextView;
    private SQLiteDatabase db;
    ImageView passwordIcon;
    private boolean passwordshowing = false;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManager = SessionManager.getInstance();
        sessionManager.setContext(getApplicationContext()); // Remplacez getApplicationContext() par le contexte approprié

        emailEditText = findViewById(R.id.email_edit_text);
        passwordEditText = findViewById(R.id.password_edit_text);
        loginBtn = findViewById(R.id.login_btn);
        progressBar = findViewById(R.id.progress_bar);
        createAccountBtnTextView = findViewById(R.id.Create_Account_text_view_btn);
        passwordIcon = findViewById(R.id.passwordIcon);
        // Instancier la base de données
        DatabaseHelper dbHelper = new DatabaseHelper(this);
        db = dbHelper.getReadableDatabase();

        passwordIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(passwordshowing){
                    passwordshowing=false;
                    passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    passwordIcon.setImageResource(R.drawable.password_show);

                }else {
                    passwordshowing=true;

                    passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    passwordIcon.setImageResource(R.drawable.password_hide);
                }
                passwordEditText.setSelection(passwordEditText.length());
            }
        });

        loginBtn.setOnClickListener((v)-> loginUser() );
        createAccountBtnTextView.setOnClickListener((v)->startActivity(new Intent(LoginActivity.this,CreateAccountActivity.class)) );
    }

    void loginUser(){
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        boolean isValidated = validateData(email,password);
        if(!isValidated){
            return;
        }

        loginAccountInSQLite(email,password);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    /* SessionManager Cette approche vous permet de centraliser la gestion de l'état de connexion dans votre
    application et de prendre les mesures appropriées en fonction de cet état dans différentes activités.
     */
    void loginAccountInSQLite(String email, String password) {
        changeInProgress(true);
        String[] selectionArgs = { email };
        Cursor cursor = db.query(Candidat.class.getSimpleName(), null, "Email = ?", selectionArgs, null, null, null);
        Cursor cursor1 = db.query(Company.class.getSimpleName(), null, "Email = ?", selectionArgs, null, null, null);
        Cursor cursor2 = db.query(Agence.class.getSimpleName(), null, "Email = ?", selectionArgs, null, null, null);

        if (cursor.moveToFirst()) {
            String passwordHash = cursor.getString(cursor.getColumnIndexOrThrow("motDepasse"));
            String role = cursor.getString(cursor.getColumnIndexOrThrow("role"));
            if (passwordHash != null && password.equals(passwordHash)) {
                // Récupérer les autres informations de l'utilisateur
                String nom = cursor.getString(cursor.getColumnIndexOrThrow("nom"));
                String prenom = cursor.getString(cursor.getColumnIndexOrThrow("prenom"));
                String nationalite = cursor.getString(cursor.getColumnIndexOrThrow("nationalite"));
                String dateString = cursor.getString(cursor.getColumnIndexOrThrow("dateNaiss"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date dateNaiss = null;
                Pattern datePattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
                if (datePattern.matcher(dateString).matches()) {
                    try {
                        dateNaiss = dateFormat.parse(dateString);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    dateNaiss = null;
                }


                String Email = cursor.getString(cursor.getColumnIndexOrThrow("Email"));
                String motDepasse = cursor.getString(cursor.getColumnIndexOrThrow("motDepasse"));
                // Fermer le curseur avant de fermer la base de données
                cursor.close();
                // Modifier cette ligne pour utiliser la classe User appropriée pour stocker les données de l'utilisateur
                Candidat utilisateur = new Candidat(nom, prenom, nationalite,dateNaiss,Email,motDepasse); // Remplacez User par votre classe représentant les données de l'utilisateur
                sessionManager.setLoggedIn(true); // Connexion réussie
                sessionManager.setCurrentUser(utilisateur); // Stocker les informations de l'utilisateur dans la session

                if(role.equals("candidat")){
                    Intent intent = new Intent(LoginActivity.this, AcceuilCActivity.class);
                    String getRole = UserRole.CANDIDAT.getValue();
                    intent.putExtra("Role",getRole);
                    startActivity(intent);
                }

            } else {
                Utility.showToast(LoginActivity.this, "Mot de passe incorrect");
            }
        } else {
            cursor.close(); // Fermer le curseur avant de fermer la base de données
            Utility.showToast(LoginActivity.this, "Utilisateur introuvable");
            changeInProgress(false);
        }


        if (cursor1.moveToFirst()) {
            String passwordHash = cursor1.getString(cursor1.getColumnIndexOrThrow("motDepasse"));
            String role = cursor1.getString(cursor1.getColumnIndexOrThrow("role"));
            if (passwordHash != null && password.equals(passwordHash)) {
                // Récupérer les autres informations de l'utilisateur
                String nom = cursor1.getString(cursor1.getColumnIndexOrThrow("name"));
                String depnom = cursor1.getString(cursor1.getColumnIndexOrThrow("departementName"));
                String subdep = cursor1.getString(cursor1.getColumnIndexOrThrow("subdepartementName"));
                String companyNationalId = cursor1.getString(cursor1.getColumnIndexOrThrow("companyNationalId"));
                String contact1 = cursor1.getString(cursor1.getColumnIndexOrThrow("contact1Name"));
                String contact2 = cursor1.getString(cursor1.getColumnIndexOrThrow("contact2Name"));
                int num = cursor1.getInt(cursor1.getColumnIndexOrThrow("numero"));

                String Email = cursor1.getString(cursor1.getColumnIndexOrThrow("Email"));
                String motDepasse = cursor1.getString(cursor1.getColumnIndexOrThrow("motDepasse"));
                // Fermer le curseur avant de fermer la base de données
                cursor1.close();
                // Modifier cette ligne pour utiliser la classe User appropriée pour stocker les données de l'utilisateur
                Company company = new Company(Email,motDepasse,nom,depnom,subdep,companyNationalId,contact1,contact2,num); // Remplacez User par votre classe représentant les données de l'utilisateur
                sessionManager.setLoggedIn(true); // Connexion réussie
                sessionManager.getInstance().setCurrentCompany(company); // Stocker les informations de l'utilisateur dans la session

                if(role.equals("entreprise")){
                    Intent intent = new Intent(LoginActivity.this, AcceuilCActivity.class);
                    String getRole = UserRole.ENTREPRISE.getValue();
                    intent.putExtra("Role",getRole);
                    startActivity(intent);
                }

            } else {
                Utility.showToast(LoginActivity.this, "Mot de passe incorrect");
            }
        } else {
            cursor1.close(); // Fermer le curseur avant de fermer la base de données
            Utility.showToast(LoginActivity.this, "Utilisateur introuvable");
            changeInProgress(false);
        }



        if (cursor2.moveToFirst()) {
            String passwordHash = cursor2.getString(cursor2.getColumnIndexOrThrow("motDepasse"));
            String role = cursor2.getString(cursor2.getColumnIndexOrThrow("role"));
            if (passwordHash != null && password.equals(passwordHash)) {
                // Récupérer les autres informations de l'utilisateur
                String nom = cursor2.getString(cursor2.getColumnIndexOrThrow("Agencename"));
                String depnom = cursor2.getString(cursor2.getColumnIndexOrThrow("departementName"));
                String subdep = cursor2.getString(cursor2.getColumnIndexOrThrow("subdepartementName"));
                String companyNationalId = cursor2.getString(cursor2.getColumnIndexOrThrow("companyNationalId"));
                String contact1 = cursor2.getString(cursor2.getColumnIndexOrThrow("contact1Name"));
                String contact2 = cursor2.getString(cursor2.getColumnIndexOrThrow("contact2Name"));
                int num = cursor2.getInt(cursor2.getColumnIndexOrThrow("numero"));

                String Email = cursor2.getString(cursor2.getColumnIndexOrThrow("Email"));
                String motDepasse = cursor2.getString(cursor2.getColumnIndexOrThrow("motDepasse"));
                // Fermer le curseur avant de fermer la base de données
                cursor2.close();
                // Modifier cette ligne pour utiliser la classe User appropriée pour stocker les données de l'utilisateur
                Agence agence = new Agence(Email,motDepasse,nom,depnom,subdep,companyNationalId,contact1,contact2,num); // Remplacez User par votre classe représentant les données de l'utilisateur
                sessionManager.setLoggedIn(true); // Connexion réussie
                sessionManager.setCurrentAgence(agence); // Stocker les informations de l'utilisateur dans la session

                if(role.equals("agence")){
                    Intent intent = new Intent(LoginActivity.this, AcceuilCActivity.class);
                    String getRole = UserRole.AGENCE.getValue();
                    intent.putExtra("Role",getRole);
                    startActivity(intent);
                }

            } else {
                Utility.showToast(LoginActivity.this, "Mot de passe incorrect");
            }
        } else {
            cursor2.close(); // Fermer le curseur avant de fermer la base de données
            Utility.showToast(LoginActivity.this, "Utilisateur introuvable");
            changeInProgress(false);
        }
    }


    private String hashPassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
            return Base64.encodeToString(hash, Base64.DEFAULT);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    void changeInProgress(boolean inProgress){
        if(inProgress){
            progressBar.setVisibility(View.VISIBLE);
            loginBtn.setVisibility(View.GONE);
        }else{
            progressBar.setVisibility(View.GONE);
            loginBtn.setVisibility(View.VISIBLE);
        }
    }

    boolean validateData(String email,String password){
        //validate the data that are input by user.

        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailEditText.setError("Email is invalid");
            return false;
        }
        if(password.length()<6){
            passwordEditText.setError("Password length is invalid, it must be above 6 characters");
            return false;
        }
        return true;
    }



}