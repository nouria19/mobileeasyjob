package com.example.easyjob.Proxy;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.easyjob.Entity.Agence;
import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Entity.Company;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class SessionManager {
    private static SessionManager instance;
    private boolean isLoggedIn = false;
    Context context;

    private SessionManager() {
        // Constructeur privé pour empêcher l'instanciation directe de la classe
    }
    public void setContext(Context context) {
        this.context = context;
    }


    public static synchronized SessionManager getInstance() {
        if (instance == null) {
            instance = new SessionManager();
        }
        return instance;
    }

    public void setLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public Candidat getCurrentUser() {

        SharedPreferences sharedPreferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);
        String nom = sharedPreferences.getString("nom", "");
        String prenom = sharedPreferences.getString("prenom", "");
        String Email = sharedPreferences.getString("Email", "");
        String nationalite = sharedPreferences.getString("nationalite", "");
        String motDepasse = sharedPreferences.getString("motDepasse", "");
        String dateString = sharedPreferences.getString("dateNaiss", "");

        // Convertir la date de naissance en objet Date
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date dateNaiss = null;
        try {
            dateNaiss = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // Autres informations de l'utilisateur à récupérer depuis la session

        return new Candidat(nom, prenom, nationalite, dateNaiss, Email, motDepasse);
    }

    public Company getCurrentCompany() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);

        String email = sharedPreferences.getString("Email", "");
        String password = sharedPreferences.getString("motDepasse", "");
        String name = sharedPreferences.getString("name", "");
        String departmentName = sharedPreferences.getString("departementName", "");
        String subDepartmentName = sharedPreferences.getString("subDepartementName", "");
        String companyNationalId = sharedPreferences.getString("companyNationalId", "");
        String contact1Name = sharedPreferences.getString("contact1Name", "");
        String contact2Name = sharedPreferences.getString("contact2Name", "");
        int numero = sharedPreferences.getInt("numero", 0);


        return new Company(email, password, name, departmentName, subDepartmentName, companyNationalId, contact1Name, contact2Name, numero);
    }


    public void setCurrentUser(Candidat utilisateur) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("nom", utilisateur.getNom());
        editor.putString("prenom", utilisateur.getPrenom());
        editor.putString("Email", utilisateur.getEmail());
        editor.putString("nationalite", utilisateur.getNationalite());
        editor.putString("motDepasse", utilisateur.getMotDepasse());

        // Vérifier si la date de naissance est nulle
        if (utilisateur.getDateNaiss() != null) {
            // Convertir la date en chaîne de caractères dans le format souhaité
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String dateString = dateFormat.format(utilisateur.getDateNaiss());
            editor.putString("dateNaiss", dateString);
        }

        editor.apply();
    }


    public void setCurrentCompany(Company company) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Email", company.getEmail());
        editor.putString("motDepasse", company.getMotDepasse());
        editor.putString("name", company.getName());
        editor.putString("departementName", company.getDepartementName());
        editor.putString("subDepartementName", company.getSubDepartementName());
        editor.putString("companyNationalId", company.getCompanyNationalId());
        editor.putString("contact1Name", company.getContact1Name());
        editor.putString("contact2Name", company.getContact2Name());
        editor.putInt("numero", company.getNumero());


        editor.apply();
    }

    public void clearCurrentUser() {
        SharedPreferences sharedPreferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public void setCurrentAgence(Agence agence) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Email", agence.getEmail());
        editor.putString("motDepasse", agence.getMotDepasse());
        editor.putString("Agencename", agence.getName());
        editor.putString("departementName", agence.getDepartementName());
        editor.putString("subDepartementName", agence.getSubDepartementName());
        editor.putString("companyNationalId", agence.getCompanyNationalId());
        editor.putString("contact1Name", agence.getContact1Name());
        editor.putString("contact2Name", agence.getContact2Name());
        editor.putInt("numero", agence.getNumero());


        editor.apply();
    }

    public Agence getCurrentAgence() {
            SharedPreferences sharedPreferences = context.getSharedPreferences("session", Context.MODE_PRIVATE);

            String email = sharedPreferences.getString("Email", "");
            String password = sharedPreferences.getString("motDepasse", "");
            String name = sharedPreferences.getString("Agencename", "");
            String departmentName = sharedPreferences.getString("departementName", "");
            String subDepartmentName = sharedPreferences.getString("subDepartementName", "");
            String companyNationalId = sharedPreferences.getString("companyNationalId", "");
            String contact1Name = sharedPreferences.getString("contact1Name", "");
            String contact2Name = sharedPreferences.getString("contact2Name", "");
            int numero = sharedPreferences.getInt("numero", 0);


            return new Agence(email, password, name, departmentName, subDepartmentName, companyNationalId, contact1Name, contact2Name, numero);

    }
}

