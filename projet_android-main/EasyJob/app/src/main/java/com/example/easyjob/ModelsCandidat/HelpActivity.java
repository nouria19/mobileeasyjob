package com.example.easyjob.ModelsCandidat;

import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.easyjob.R;

public class HelpActivity extends AppCompatActivity {

    private TextView helpTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        helpTextView = findViewById(R.id.textViewHelp);

        // Définir le contenu d'aide spécifique à votre application
        String helpContent = "\"Besoin d'aide pour rédiger votre CV et lettre de motivation ? Consultez notre guide complet en cliquant \" +\n" +
                "                \"<a href='https://www.canva.com'>ici</a>.\"";

        // Activer les liens cliquables dans le TextView
        helpTextView.setMovementMethod(LinkMovementMethod.getInstance());

        // Convertir le contenu en format HTML pour activer les liens
        Spanned spannedContent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            spannedContent = Html.fromHtml(helpContent, Html.FROM_HTML_MODE_LEGACY);
        }
        helpTextView.setText(spannedContent);
    }
}
