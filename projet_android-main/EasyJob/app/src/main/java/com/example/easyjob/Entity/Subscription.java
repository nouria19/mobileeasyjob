package com.example.easyjob.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Subscription implements Parcelable {

    private Long Id;
    private String type;
    private String condition_use;
    private String benefits;
    private int price;
    private String desabonneCond;



    public Subscription() {
    }

    protected Subscription(Parcel in) {
        if (in.readByte() == 0) {
            Id = null;
        } else {
            Id = in.readLong();
        }
        condition_use = in.readString();
        benefits = in.readString();
        price = in.readInt();
        desabonneCond = in.readString();
        type = in.readString();
    }

    public static final Creator<Subscription> CREATOR = new Creator<Subscription>() {
        @Override
        public Subscription createFromParcel(Parcel in) {
            return new Subscription(in);
        }

        @Override
        public Subscription[] newArray(int size) {
            return new Subscription[size];
        }
    };

    public String getCondition_use() {
        return condition_use;
    }

    public void setCondition_use(String condition_use) {
        this.condition_use = condition_use;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getDesabonneCond() {
        return desabonneCond;
    }

    public void setDesabonneCond(String desabonneCond) {
        this.desabonneCond = desabonneCond;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getBenefits() {
        return benefits;
    }

    public void setBenefits(String benefits) {
        this.benefits = benefits;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        if (Id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(Id);
        }
        parcel.writeString(condition_use);
        parcel.writeString(benefits);
        parcel.writeInt(price);
        parcel.writeString(desabonneCond);
        parcel.writeString(type);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
