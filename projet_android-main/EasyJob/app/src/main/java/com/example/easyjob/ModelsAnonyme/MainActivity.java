package com.example.easyjob.ModelsAnonyme;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.preference.PreferenceManager;

import android.os.Bundle;

import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.example.easyjob.Entity.DatabaseHelper;
import com.example.easyjob.Entity.Offre;
import com.example.easyjob.Entity.OffreAdapter;
import com.example.easyjob.Proxy.OffreService;
import com.example.easyjob.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// Acceuil user Anonyme
public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION_PERMISSION = 1;
    private ActivityResultLauncher<Intent> voiceRecognitionLauncher;
    private static final int REQUEST_VOICE_RECOGNITION = 1;


    private SearchView searchView;

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private OffreAdapter offreAdapter;

    private List<Offre> allOffres = new ArrayList<>();
    private SQLiteDatabase db;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.context= MainActivity.this;
        recyclerView = findViewById(R.id.recycler_view);
        String BASE_URL = "http://192.168.2.201:8081/";

        progressBar = findViewById(R.id.progressBar);

        progressBar.setVisibility(View.GONE);

        // Instancier la base de données
        DatabaseHelper dbHelper = new DatabaseHelper(this);
        db = dbHelper.getReadableDatabase();

        // Récupérer toutes les offres et les stocker dans une liste
        allOffres = setupRecyclerView();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();

        OffreService offreService = retrofit.create(OffreService.class);
        Call<List<Offre>> call = offreService.getAllOffres();
        call.enqueue(new Callback<List<Offre>>() {
            @Override
            public void onResponse(Call<List<Offre>> call, Response<List<Offre>> response) {
                if (response.isSuccessful()) {
                    List<Offre> offres = response.body();
                    if (offres != null) {
                        for (Offre offre : offres) {
                            // Afficher les détails de chaque offre
                            Log.d("Offre", "Titre: " + offre.getTitreOffre());
                            Log.d("Offre", "Description: " + offre.getDescription());
                            Log.d("Offre", "Ville: " + offre.getVille());
                            Log.d("Offre", "Code Postal: " + offre.getCodePostal());
                            Log.d("Offre", "Salaire: " + offre.getSalaire());
                            Log.d("Offre", "Rue: " + offre.getRue());
                            Log.d("Offre", "Type Contrat:" + offre.getTypeContrat());
                            Log.d("Offre", "Numero:" + offre.getNuméro());
                            Log.d("Offre", "Date offre:" + offre.getDateOffre());
                            Log.d("Offre", "Date publication:" + offre.getDatePublication());

                            Log.d("Offre", "--------------------------------");
                        }
                    }
                } else {
                    Log.e("Erreur", "Code d'erreur : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Offre>> call, Throwable t) {
                Log.e("Erreur", "Échec de la requête : " + t.getMessage());
            }
        });


        // Configurer le RecyclerView avec l'adapter et le layout manage
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        offreAdapter = new OffreAdapter(allOffres, context);
        recyclerView.setAdapter(offreAdapter);


        // Demander la permission d'accéder à la localisation
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_LOCATION_PERMISSION);

        // Initialiser le lanceur d'activité pour la reconnaissance vocale
        voiceRecognitionLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK && result.getData() != null) {
                        // Votre logique de traitement des résultats ici
                    }
                });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);

        // Récupération de l'objet SearchView
        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) searchItem.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                progressBar.setVisibility(View.VISIBLE);
                // Appel de votre méthode de récupération de données ici en utilisant la chaîne de recherche "query"
                List<Offre> dataList = getData(query);
                // Mise à jour des données de l'adapter existant
                offreAdapter.updateData(dataList);

                // Masquer la barre de progression
                progressBar.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Réinitialisation de l'interface utilisateur ici si nécessaire
                if (TextUtils.isEmpty(newText)) { // Vérifier si la chaîne de recherche est vide
                    offreAdapter.updateData(allOffres); // Afficher toutes les offres
                }
                return true;
            }
        });

        // Trouver l'élément du menu de recherche vocale
        MenuItem voiceSearchItem = menu.findItem(R.id.action_voice_search);

        // Ajouter un listener de clic sur l'icône du microphone
        voiceSearchItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                // Lancer la reconnaissance vocale
                startVoiceRecognition();
                return true;
            }

        });

        return super.onCreateOptionsMenu(menu);
    }

    // Exemple de code pour traiter les résultats de la reconnaissance vocale
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_VOICE_RECOGNITION && resultCode == RESULT_OK && data != null) {
            ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (results != null && results.size() > 0) {
                String spokenText = results.get(0); // Récupérer le texte parlé
                // Effectuer la recherche dans la base de données SQLite en utilisant le texte parlé
                // et mettre à jour l'interface utilisateur en conséquence
                searchInDatabase(spokenText);
            }
        }
    }
    @SuppressLint("Range")
    private void searchInDatabase(String query) {
        // Effectuer la recherche dans la base de données SQLite en utilisant la valeur de query
        // et mettre à jour l'interface utilisateur en conséquence

        // Exemple de requête SQL pour filtrer les données dans la table "offre"
        String sql = "SELECT * FROM Offre WHERE titreOffre LIKE '%" + query + "%' OR typeContrat LIKE '%" + query + "%' OR Ville LIKE '%" + query + "%' OR CodePostal LIKE '%" + query + "%' OR numéro LIKE '%" + query + "%' OR Salaire LIKE '%" + query + "%' OR rue LIKE '%" + query + "%' OR description LIKE '%" + query + "%'";


        // Exécuter la requête SQL et récupérer le curseur de résultats
        Cursor cursor = db.rawQuery(sql, null);

        // Traiter les résultats du curseur de résultats
        if (cursor != null && cursor.moveToFirst()) {
            // Exemple de code pour effacer les données actuellement affichées dans le RecyclerView
            allOffres.clear();

            // Exemple de code pour ajouter les données filtrées à la liste de données utilisée pour alimenter le RecyclerView
            while (!cursor.isAfterLast()) {
                // Récupérer les valeurs des colonnes de la ligne courante du curseur

                String titre = cursor.getString(cursor.getColumnIndex("titreOffre"));
                String typeC= cursor.getString(cursor.getColumnIndex("typeContrat"));
                String ville= cursor.getString(cursor.getColumnIndex("Ville"));
                int codep= cursor.getInt(cursor.getColumnIndex("CodePostal"));
                int num= cursor.getInt(cursor.getColumnIndex("numéro"));
                int salaire=cursor.getInt(cursor.getColumnIndex("Salaire"));
                String rue= cursor.getString(cursor.getColumnIndex("rue"));
                String desc= cursor.getString(cursor.getColumnIndex("description"));
                Date dateO= new Date(cursor.getLong(cursor.getColumnIndex("dateOffre")));
                Timestamp dateP= new Timestamp(cursor.getLong(cursor.getColumnIndex("datePublication")));


                // Créer un objet Offre avec les valeurs récupérées
                Offre offre = new Offre(titre,ville,codep,dateO,salaire,num,rue,typeC,desc,dateP); // Remplacez ... avec les autres valeurs de colonnes nécessaires

                // Ajouter l'objet Offre à la liste de données
                allOffres.add(offre);

                // Passer à la ligne suivante du curseur
                cursor.moveToNext();
            }

            // Exemple de code pour mettre à jour l'adapter du RecyclerView
            offreAdapter = new OffreAdapter(allOffres,this); // initialisation de l'adaptateur avec la liste des offres proches
            recyclerView.setAdapter(offreAdapter);
            offreAdapter.notifyDataSetChanged();
        }
        // Fermer le curseur de résultats
        if (cursor != null) {
            cursor.close();
        }
    }

    // Fonction pour lancer la reconnaissance vocale
    private void startVoiceRecognition() {
        // Créer l'intent pour lancer la reconnaissance vocale
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Parlez maintenant");
        // Lancer l'activité de reconnaissance vocale
        voiceRecognitionLauncher.launch(intent);
    }

    @SuppressLint("Range")
    private List<Offre> getData(String query) {
        // Exécuter une requête SQL pour récupérer les offres correspondant au texte de recherche
        String newText = query.trim();
        String sql = "SELECT * FROM Offre WHERE titreOffre LIKE ? OR Ville LIKE ?";
        Cursor cursor = db.rawQuery(sql, new String[]{"%" + newText + "%", "%" + newText + "%"});


        // Parcourir les résultats de la requête et créer une liste d'objets Offre
        List<Offre> offres = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Offre offre = new Offre();
                offre.setTitreOffre(cursor.getString(cursor.getColumnIndex("titreOffre")));
                offre.setTypeContrat(cursor.getString(cursor.getColumnIndex("typeContrat")));
                offre.setVille(cursor.getString(cursor.getColumnIndex("Ville")));
                offre.setCodePostal(cursor.getInt(cursor.getColumnIndex("CodePostal")));
                offre.setNumero(cursor.getInt(cursor.getColumnIndex("numéro")));
                offre.setSalaire(cursor.getInt(cursor.getColumnIndex("Salaire")));
                offre.setRue(cursor.getString(cursor.getColumnIndex("rue")));
                offre.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                offre.setDateOffre(new Date(cursor.getLong(cursor.getColumnIndex("dateOffre"))));
                offre.setDatePublication(new Timestamp(cursor.getLong(cursor.getColumnIndex("datePublication"))));


                offres.add(offre);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return offres;
    }

        @Override
        protected void onDestroy () {
            super.onDestroy();
            db.close();
        }

        @SuppressLint("Range")
        private List<Offre> setupRecyclerView () {

            // Exécuter la requête SQL pour récupérer les offres
            String query = "SELECT * FROM Offre";
            Cursor cursor = db.rawQuery(query, null);

            // Parcours des résultats de la requête et création d'une liste d'objets Offre
            // on crée la classe Offre pour faciliter la manipulation
            // et le transfert de ces données dans l'application.
            List<Offre> offres = new ArrayList<>();
            if (cursor.moveToFirst()) {
                do {
                    Offre offre = new Offre();
                    offre.setTitreOffre(cursor.getString(cursor.getColumnIndex("titreOffre")));
                    offre.setTypeContrat(cursor.getString(cursor.getColumnIndex("typeContrat")));
                    offre.setVille(cursor.getString(cursor.getColumnIndex("Ville")));
                    offre.setCodePostal(cursor.getInt(cursor.getColumnIndex("CodePostal")));
                    offre.setNumero(cursor.getInt(cursor.getColumnIndex("numéro")));
                    offre.setSalaire(cursor.getInt(cursor.getColumnIndex("Salaire")));
                    offre.setRue(cursor.getString(cursor.getColumnIndex("rue")));
                    offre.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                    offre.setDateOffre(new Date(cursor.getLong(cursor.getColumnIndex("dateOffre"))));
                    offre.setDatePublication(new Timestamp(cursor.getLong(cursor.getColumnIndex("datePublication"))));


                    offres.add(offre);
                } while (cursor.moveToNext());
            }
            cursor.close();
            return offres;
        }


        @Override
        public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults){
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (requestCode == REQUEST_LOCATION_PERMISSION) {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    // L'utilisateur a accepté la permission d'accéder à la localisation
                    Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();

                    // Enregistrer la permission d'accéder à la localisation dans les préférences de l'utilisateur
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putBoolean("location_permission", true);
                    editor.apply();

                    // Récupérer la position de l'utilisateur
                    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        double latitude = location.getLatitude();
                        double longitude = location.getLongitude();


                        // Filtrer les annonces en fonction de la position de l'utilisateur

                        List<Offre> OffreProches = new ArrayList<>();
                        for (Offre Offre : allOffres) {
                            String adress = Offre.getNumero() + Offre.getRue() + Offre.getVille() + Offre.getCodePostal();
                            if (estProche(latitude, longitude, adress)) {
                                OffreProches.add(Offre);
                            }
                        }
                        offreAdapter = new OffreAdapter(OffreProches,this); // initialisation de l'adaptateur avec la liste des offres proches
                        recyclerView.setAdapter(offreAdapter);
                        offreAdapter.notifyDataSetChanged(); //

                    } else {

                        // L'utilisateur a refusé la permission d'accéder à la localisation
                        //Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();

                        // Sélectionner les annonces les plus récentes

                        List<Offre> OffreRecentes = new ArrayList<>();

                        // Tri des annonces par ordre décroissant de date de publication
                        Collections.sort(allOffres, new Comparator<Offre>() {
                            @Override
                            public int compare(Offre o1, Offre o2) {
                                return o2.getDateOffre().compareTo(o1.getDateOffre());
                            }
                        });
                        // Récupérer les 10 annonces les plus récentes
                        for (int i = 0; i < Math.min(10, allOffres.size()); i++) {
                            OffreRecentes.add(allOffres.get(i));
                        }
                        offreAdapter = new OffreAdapter(OffreRecentes,this);
                        offreAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(offreAdapter);
                    }
                }
            }
        }

        private List<Offre> filter (List < Offre > offres, String query){
            query = query.toLowerCase();
            List<Offre> filteredList = new ArrayList<>();
            for (Offre offre : offres) {
                String titre = offre.getTitreOffre().toLowerCase();
                String ville = offre.getVille().toLowerCase();
                if (titre.contains(query) || ville.contains(query)) {
                    filteredList.add(offre);
                }
            }
            return filteredList;
        }

        // Méthode pour déterminer si une offre est proche de la position de l'utilisateur
        public boolean estProche ( double latitude, double longitude, String adressePostal){
            // Obtenir la position de l'adresse complète recherchée
            Geocoder geocoder = new Geocoder(this);
            try {
                List<Address> addresses = geocoder.getFromLocationName(adressePostal, 1);
                if (addresses.isEmpty()) {
                    return false;
                }
                Address address = addresses.get(0);
                double lat = address.getLatitude();
                double lon = address.getLongitude();

                // Calculer la distance entre la position de l'utilisateur et la position de la ville recherchée
                Location locationUser = new Location("");
                locationUser.setLatitude(latitude);
                locationUser.setLongitude(longitude);

                Location locationVille = new Location("");
                locationVille.setLatitude(lat);
                locationVille.setLongitude(lon);

                float distance = locationUser.distanceTo(locationVille) / 1000; // distance en km
                return distance <= 50; // la ville est considérée comme proche si elle est à moins de 50 km
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }


    }

