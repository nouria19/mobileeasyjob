package com.example.easyjob.ModelsAnonyme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.easyjob.Entity.Offre;
import com.example.easyjob.ModelsCandidat.CandidatureActivity;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class OffreDetailsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private TextView titreOffreTextView, numeroTextView, rueTextView, villeTextView, codePostalTextView, salaireTextView, typeContratTextView, descriptionTextView,dateOffreTextView;
    Button postuler;
    Button signaler;
    ImageView shareButton;
    ImageView homeButton;

    // Déclarer un champ pour la carte
    private GoogleMap googleMap;
    private MapView mapView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Mapbox.getInstance(this,getString(R.string.access_token));
        setContentView(R.layout.activity_offre_details);

        // Obtenir une référence à la carte
        /*mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);*/

        // Récupération des vues
        titreOffreTextView = findViewById(R.id.titre_offre);
        numeroTextView = findViewById(R.id.numero);
        rueTextView = findViewById(R.id.rue);
        villeTextView = findViewById(R.id.ville);
        codePostalTextView = findViewById(R.id.code_postal);
        salaireTextView = findViewById(R.id.salaire);
        typeContratTextView = findViewById(R.id.type_contrat);
        dateOffreTextView = findViewById(R.id.date_depot);
        descriptionTextView = findViewById(R.id.description);
        postuler= findViewById(R.id.button);
        signaler= findViewById(R.id.button_signaler);
        shareButton = findViewById(R.id.share_icon);
        homeButton=findViewById(R.id.home_icon);



        // Récupération de l'intent qui a lancé cette activité
        Intent intent = getIntent();

        // Récupération des données envoyées depuis l'activité précédente
        String titreOffre = intent.getStringExtra("titreOffre");
        int numero =  intent.getIntExtra("numéro", 0);

        String rue = intent.getStringExtra("rue");
        String ville = intent.getStringExtra("Ville");

        int codePostalInt = intent.getIntExtra("code postal", 0); // Remplacez 0 par la valeur par défaut que vous souhaitez fournir si la clé n'est pas présente dans l'Intent
        String codePostal = Integer.toString(codePostalInt);


        int Salaire = intent.getIntExtra("Salaire", 0);

        String typeContrat = intent.getStringExtra("Type contrat");


        String description = intent.getStringExtra("description");

        Date dateOffre = (Date) intent.getSerializableExtra("dateOffre"); // Récupère la date comme un objet Date
        String formatSouhaite = "yyyy-MM-dd"; // Spécifiez le format de date souhaité
        SimpleDateFormat sdf = new SimpleDateFormat(formatSouhaite, Locale.FRANCE); // Crée un objet SimpleDateFormat avec le format souhaité
        String dateOffreString = sdf.format(dateOffre); // Convertit la date en chaîne de caractères dans le format souhaité


        // Affichage des données dans les TextView correspondantes
        titreOffreTextView.setText(titreOffre);
        numeroTextView.setText(String.valueOf(numero));
        rueTextView.setText(rue);
        villeTextView.setText(ville);
        codePostalTextView.setText(codePostal);
        salaireTextView.setText(String.valueOf(Salaire));
        typeContratTextView.setText(typeContrat);
        descriptionTextView.setText(description);
        dateOffreTextView.setText(dateOffreString);

        // Vérifie si l'utilisateur est connecté
        if (!SessionManager.getInstance().isLoggedIn()) {
            postuler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(OffreDetailsActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            signaler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(OffreDetailsActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            });

            homeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(OffreDetailsActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            postuler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(OffreDetailsActivity.this, CandidatureActivity.class);
                    startActivity(intent);
                }
            });

            signaler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Gérer l'action de signalement pour l'utilisateur connecté
                }
            });

            homeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Gérer l'action du bouton Accueil pour l'utilisateur connecté
                }
            });
        }


        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String deepLinkUrl = createDeepLinkUrl();
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, deepLinkUrl);
                startActivity(Intent.createChooser(shareIntent, "Share activity URL"));
            }

        });

    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;
        /*String adress = Offre.getNumero() + rue + Offre.getVille() + Offre.getCodePostal();

        // Ajouter un marqueur à l'adresse de l'offre
        LatLng adresse = new LatLng(LATITUDE_DE_L_ADRESSE, LONGITUDE_DE_L_ADRESSE);
        googleMap.addMarker(new MarkerOptions().position(adresse));

        // Centrer la carte sur l'adresse
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(adresse, 12));*/
    }

    @Override
    public void onResume() {
        super.onResume();
        //mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
       // mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
       // mapView.onLowMemory();
    }


    private String createDeepLinkUrl() {
        String packageName = getPackageName();
        String activityName = OffreDetailsActivity.class.getSimpleName();
        String deepLinkUri = "https://easy.com/details_offre";
        String deepLinkUrl = "android-app://" + packageName + "/" + activityName + "?deeplink=" + deepLinkUri;
        return deepLinkUrl;
    }

}
