package com.example.easyjob.ModelsEmployeur;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Entity.Company;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;

import java.util.Date;

public class ProfilEActivity extends AppCompatActivity {

    private TextView textViewname;
    private TextView companyNationalId; // nom+prenom
    private TextView numero;
    private TextView Email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_eactivity);

        SessionManager sessionManager = SessionManager.getInstance();
        sessionManager.setContext(getApplicationContext());

        // Récupérer les références des vues
        textViewname = findViewById(R.id.name);
        companyNationalId = findViewById(R.id.companyNationalId);
        numero = findViewById(R.id.numero);
        Email = findViewById(R.id.Email);

        if (sessionManager.isLoggedIn()) {
            Company utilisateur = sessionManager.getCurrentCompany();

            // Récupérer les informations de l'utilisateur
            String nom = utilisateur.getName();
            String prenom = utilisateur.getCompanyNationalId();
            String email = utilisateur.getEmail();
            int nationalite = utilisateur.getNumero();


            // Mettre à jour les vues avec les informations de l'utilisateur
            TextView textViewNomPrenom = findViewById(R.id.name);
            TextView textViewEmail = findViewById(R.id.Email);
            TextView textViewNationalite = findViewById(R.id.numero);
            TextView textViewTel = findViewById(R.id.companyNationalId);

            textViewNomPrenom.setText(nom);
            textViewEmail.setText(email);
            textViewNationalite.setText("Phone Number: " + nationalite);
            textViewTel.setText("Company National ID: " + prenom);

        }

    }


    }
