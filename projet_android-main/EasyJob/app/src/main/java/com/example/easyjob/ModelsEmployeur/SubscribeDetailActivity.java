package com.example.easyjob.ModelsEmployeur;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.easyjob.Entity.DatabaseHelper;

import com.example.easyjob.Entity.Subscription;

import com.example.easyjob.ModelsAnonyme.OTPVerification;
import com.example.easyjob.R;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.paymentsheet.PaymentSheet;
import com.stripe.android.paymentsheet.PaymentSheetResult;

import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscribeDetailActivity extends AppCompatActivity {
    private TextView conduse, cond, benefits, benef, price, prix, condes, conds;
    Button payment;
    private List<Subscription> InfoSub = new ArrayList<>();
    private SQLiteDatabase db;


    String SECRET_KEY = "sk_test_51MywRALciYzCLfEAs6LbOEEpCaauGYyuBhe90ZKtlgtDgeVAMIF74RiOeiYPHUL0SQxz1pAE5IMvsEJJ2sLndRkf00UGcJc7nI";
    String PUBLISH_KEY = "pk_test_51MywRALciYzCLfEAh3TvG4YpoPvrgTJKG5L8uHJcA5TWuwFKaG76GWXCHVHSNvZDng9JrJTEB0psG2GoLuCGSB1O00ykQheJHs";
    PaymentSheet paymentSheet;
    String customerID;
    String EphericalKey;
    String ClientSecret;
    String getEmail,getpassword,getname,getdepartementName,getsubDepartementName,getcompanyNationalId,getcontact1Name,getcontact2Name
            ,getRole;

    int getnumero;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscribe_detail);


        // Récupération de l'intent qui a lancé cette activité
        Intent intent = getIntent();


        // Instancier la base de données
        DatabaseHelper dbHelper = new DatabaseHelper(this);
        db = dbHelper.getReadableDatabase();
        setupRecyclerView(intent.getStringExtra("type"));
        displaySubscriptionData();


        conduse = findViewById(R.id.conduse);
        cond = findViewById(R.id.cond);
        benefits = findViewById(R.id.benefits);
        benef = findViewById(R.id.benef);
        price = findViewById(R.id.price);
        prix = findViewById(R.id.prix);
        condes = findViewById(R.id.condes);
        conds = findViewById(R.id.conds);


        getRole = getIntent().getStringExtra("Role");
        if(getRole.equals("entreprise")) {

            getEmail = getIntent().getStringExtra("Email");
            getpassword = getIntent().getStringExtra("password");
            getname = getIntent().getStringExtra("CompanyName");
            getdepartementName = getIntent().getStringExtra("departementName");
            getsubDepartementName = getIntent().getStringExtra("subDepartementName");
            getcompanyNationalId = getIntent().getStringExtra("companyNationalId");
            getcontact1Name = getIntent().getStringExtra("contact1Name");
            getcontact2Name = getIntent().getStringExtra("contact2Name");
            getnumero = getIntent().getIntExtra("numero",0);

        }else if(getRole.equals("agence")){
            getEmail = getIntent().getStringExtra("Email");
            getpassword = getIntent().getStringExtra("password");
            getname = getIntent().getStringExtra("Agencename");
            getdepartementName = getIntent().getStringExtra("departementName");
            getsubDepartementName = getIntent().getStringExtra("subDepartementName");
            getcompanyNationalId = getIntent().getStringExtra("companyNationalId");
            getcontact1Name = getIntent().getStringExtra("contact1Name");
            getcontact2Name = getIntent().getStringExtra("contact2Name");
            getnumero = getIntent().getIntExtra("numero",0);


        }


        payment = findViewById(R.id.button);
        PaymentConfiguration.init(this, PUBLISH_KEY);

        paymentSheet = new PaymentSheet(this, paymentSheetResult -> {
            onPaymentResult(paymentSheetResult);
        });

        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PaymentFlow();
            }
        });

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/customers",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            customerID = object.getString("id");
                            getEphericalKey(customerID);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Gérer l'erreur
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + SECRET_KEY);
                return header;
            }
        };

        int socketTimeout = 50000; // 30 seconds
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void displaySubscriptionData() {
        if (!InfoSub.isEmpty()) {
            Subscription subscription = InfoSub.get(0);

            cond.setText(subscription.getCondition_use());
            benef.setText(subscription.getBenefits());
            prix.setText(String.valueOf(subscription.getPrice()));
            conds.setText(subscription.getDesabonneCond());


            // Affichage des éléments séparés par des puces
            String[] benefitItems = subscription.getBenefits().split(",");
            String formattedBenefits = "<ul><li>" + TextUtils.join("</li><li>", benefitItems) + "</li></ul>";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                benef.setText(Html.fromHtml(formattedBenefits, Html.FROM_HTML_MODE_COMPACT));
            }
        }
    }

    @SuppressLint("Range")
    private List<Subscription> setupRecyclerView(String type) {
        String query = "SELECT * FROM Subscription WHERE type = ?";
        Cursor cursor = db.rawQuery(query, new String[]{type});

        InfoSub.clear(); // Vide la liste pour éviter les doublons

        if (cursor.moveToFirst()) {
            do {
                Subscription sub = new Subscription();
                sub.setCondition_use(cursor.getString(cursor.getColumnIndex("condition_use")));
                sub.setBenefits(cursor.getString(cursor.getColumnIndex("benefits")));
                sub.setPrice(cursor.getInt(cursor.getColumnIndex("price")));
                sub.setDesabonneCond(cursor.getString(cursor.getColumnIndex("desabonneCond")));

                InfoSub.add(sub);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return InfoSub;
    }


    private void onPaymentResult(PaymentSheetResult paymentSheetResult) {
        if (paymentSheetResult instanceof PaymentSheetResult.Completed) {
            Toast.makeText(this, "Payment successful", Toast.LENGTH_SHORT).show();

        if(getRole.equals("entreprise")){
            Intent intent=new Intent(SubscribeDetailActivity.this, OTPVerification.class);
            intent.putExtra("Email",getEmail);
            intent.putExtra("password",getpassword);
            intent.putExtra("Company_Name",getname);
            intent.putExtra("departementName",getdepartementName);
            intent.putExtra("subDepartementName",getsubDepartementName);
            intent.putExtra("companyNationalId",getcompanyNationalId);
            intent.putExtra("contact1Name",getcontact1Name);
            intent.putExtra("contact2Name",getcontact2Name);
            intent.putExtra("numero",getnumero);
            intent.putExtra("Role", getRole);
            startActivity(intent);
        }else if(getRole.equals("agence")){
            Intent intent=new Intent(SubscribeDetailActivity.this, OTPVerification.class);
            intent.putExtra("Email",getEmail);
            intent.putExtra("password",getpassword);
            intent.putExtra("Agencenameame",getname);
            intent.putExtra("departementName",getdepartementName);
            intent.putExtra("subDepartementName",getsubDepartementName);
            intent.putExtra("companyNationalId",getcompanyNationalId);
            intent.putExtra("contact1Name",getcontact1Name);
            intent.putExtra("contact2Name",getcontact2Name);
            intent.putExtra("numero",getnumero);
            intent.putExtra("Role", getRole);
            startActivity(intent);
        }
        }
    }

    private void getEphericalKey(String customerID) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/ephemeral_keys",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            EphericalKey = object.getString("id");
                            getClientSecret(customerID, EphericalKey);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Gérer l'erreur
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + SECRET_KEY);
                header.put("Stripe-Version", "2022-11-15");
                return header;
            }

            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("customer", customerID);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(SubscribeDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    private void getClientSecret(String customerID, String ephericalKey) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://api.stripe.com/v1/payment_intents",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            ClientSecret = object.getString("client_secret");
                            PaymentFlow(); // Appel de la méthode PaymentFlow() ici
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Gérer l'erreur
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("Authorization", "Bearer " + SECRET_KEY);
                return header;
            }

            @Override
            public Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("customer", customerID);
                params.put("amount", "1000" + "00");
                params.put("currency", "eur");
                params.put("automatic_payment_methods[enabled]", "true");
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(SubscribeDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    private void PaymentFlow() {
        if (ClientSecret != null) {
            paymentSheet.presentWithPaymentIntent(
                    ClientSecret, new PaymentSheet.Configuration("EasyJob",
                            new PaymentSheet.CustomerConfiguration(
                                    customerID,
                                    EphericalKey
                            ))
            );
        } else {
            // Gérer le cas où ClientSecret est null
        }
    }
}


