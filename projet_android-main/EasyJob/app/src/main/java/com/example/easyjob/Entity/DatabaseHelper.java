package com.example.easyjob.Entity;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Base64;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {
    /*
    cette classe est utile pour gérer les opérations liées à la base de données SQLite dans une application Android.
     */
    private static final String DATABASE_NAME = "Emploi.db";
    private static final int DATABASE_VERSION = 1;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Offre (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "titreOffre TEXT, " +
                "Ville TEXT, " +
                "CodePostal INTEGER, " +
                "dateOffre DATE, " +
                "numéro INTEGER, " +
                "rue TEXT, " +
                "typeContrat TEXT, " +
                "description TEXT, " +
                "Salaire INTEGER DEFAULT NULL, " +
                "datePublication TIMESTAMP, "+
                "candidatures TEXT" +
                ")");

        db.execSQL("CREATE TABLE Candidat (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nom VARCHAR(35)," +
                "prenom VARCHAR(35)," +
                "nationalite VARCHAR(255)," +
                "dateNaiss DATE," +
                "Email VARCHAR(255)," +
                "motDepasse VARCHAR(55)," +
                "role VARCHAR(35)" +
                ")");

        db.execSQL("CREATE TABLE Company (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "name VARCHAR(35)," +
                "departementName VARCHAR(35)," +
                "subDepartementName VARCHAR(255)," +
                "companyNationalId VARCHAR(255)," +
                "contact1Name VARCHAR(55)," +
                "contact2Name VARCHAR(35)," +
                "numero INTEGER,"+
                "Email VARCHAR(255)," +
                "motDepasse VARCHAR(55)," +
                "role VARCHAR(35)," +
                "offres TEXT DEFAULT NULL" +
                ")");

        db.execSQL("CREATE TABLE Agence (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "Agencename VARCHAR(35)," +
                "departementName VARCHAR(35)," +
                "subDepartementName VARCHAR(255)," +
                "companyNationalId VARCHAR(255)," +
                "contact1Name VARCHAR(55)," +
                "contact2Name VARCHAR(35)," +
                "numero INTEGER,"+
                "Email VARCHAR(255)," +
                "motDepasse VARCHAR(55)," +
                "role VARCHAR(35)," +
                "offres TEXT DEFAULT NULL" +
                ")");

        db.execSQL("CREATE TABLE Subscription (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "condition_use VARCHAR(255)," +
                "benefits VARCHAR(255)," +
                "price INTEGER," +
                "desabonneCond VARCHAR(255),"+
                "type VARCHAR(30)"+
                ")");

        db.execSQL("CREATE TABLE Candidature (" +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nom VARCHAR(30)," +
                "prenom VARCHAR(30)," +
                "dateNaissance DATE," +
                "nationalite VARCHAR(25),"+
                "cv BLOB," +
                "lettreMotivation BLOB" +
                ")");


        ContentValues value4 = new ContentValues();
        value4.put("condition_use","Access to all premium features for a duration of 1 month.");
        value4.put("benefits","Unlimited downloads, ad-free experience, priority customer support.");
        value4.put("price","1000,00£");
        value4.put("desabonneCond","You can cancel the subscription anytime before the next billing cycle to avoid further charges.");
        value4.put("type","One-time subscription");

        ContentValues value5 = new ContentValues();
        value5.put("condition_use", "Access to all premium features for a duration of 30 days.");
        value5.put("benefits", "Unlimited downloads, exclusive content, priority customer support.");
        value5.put("price", "1000.00£");
        value5.put("desabonneCond", "You can cancel the subscription anytime before the next billing cycle to avoid further charges.");
        value5.put("type", "One-month subscription");


        ContentValues value6 = new ContentValues();
        value6.put("condition_use", "Access to premium features for the duration of the subscribed term.");
        value6.put("benefits", "Full access to all features, personalized recommendations, priority customer support.");
        value6.put("price", "1000.00£");
        value6.put("desabonneCond", "You can cancel the subscription anytime before the next billing cycle to avoid further charges.");
        value6.put("type", "One-term subscription");


        ContentValues value7 = new ContentValues();
        value7.put("condition_use", "Access to premium features for a duration of six months.");
        value7.put("benefits", "Unlimited access to all features, exclusive content, priority customer support.");
        value7.put("price", "1000.00£");
        value7.put("desabonneCond", "You can cancel the subscription anytime before the next billing cycle to avoid further charges.");
        value7.put("type", "One-semester subscription");

        ContentValues value8 = new ContentValues();
        value8.put("condition_use", "Access to premium features for a duration of one year.");
        value8.put("benefits", "Full access to all features, exclusive content, priority customer support.");
        value8.put("price", "1000.00£");
        value8.put("desabonneCond", "You can cancel the subscription anytime before the next billing cycle to avoid further charges.");
        value8.put("type", "One-year subscription");


        ContentValues value9 = new ContentValues();
        value9.put("condition_use","");
        value9.put("benefits","");
        value9.put("price","");
        value9.put("desabonneCond","");
        value9.put("type","indefinite subscription");




        // Insertion d'un utilisateur avec mot de passe haché
        ContentValues values = new ContentValues();
        values.put("nom", "Doe");
        values.put("prenom", "John");
        values.put("nationalite", "américaine");
        values.put("dateNaiss", "2000-01-01");
        values.put("Email", "johndoe@example.com");
        values.put("motDepasse","password123");
        values.put("role",UserRole.CANDIDAT.getValue());
        db.insert("Candidat",null,values);


        ContentValues values1 = new ContentValues();
        values1.put("titreOffre", "Développeur Android");
        values1.put("numéro", 75);
        values1.put("rue", "Augustin Fliche");
        values1.put("Ville", "Paris");
        values1.put("CodePostal", 75000);
        values1.put("dateOffre", "2022-04-01");
        values1.put("typeContrat", "CDI");
        values1.put("description", "Recherche un développeur Android pour rejoindre notre équipe");
        values1.putNull("Salaire");
        db.insert("Offre", null, values1);

        ContentValues values2 = new ContentValues();
        values2.put("titreOffre", "Développeur Android");
        values2.put("numéro", 75);
        values2.put("rue", "Augustin Fliche");
        values2.put("Ville", "Montpellier");
        values2.put("CodePostal", 34090);
        values2.put("dateOffre", "2023-03-30");
        values2.put("typeContrat", "CDI");
        values2.put("description", "Recherche un développeur Android pour rejoindre notre équipe");
        values2.putNull("Salaire");
        db.insert("Offre", null, values2);

        ContentValues values3 = new ContentValues();
        values3.put("titreOffre", "full stack");
        values3.put("numéro", 75);
        values3.put("rue", "Augustin Fliche");
        values3.put("Ville", "Montpellier");
        values3.put("CodePostal", 34090);
        values3.put("dateOffre", "2023-03-30");
        values3.put("typeContrat", "CDI");
        values3.put("description", "Recherche un développeur Android pour rejoindre notre équipe");
        values3.putNull("Salaire");
        db.insert("Offre", null, values3);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }


    // Fonction pour hacher le mot de passe avec SHA-256
    private String hashPassword(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
            return Base64.encodeToString(hash, Base64.DEFAULT);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addUser(String email, String password, String nom, String prenom, Date dateNaiss, String nationalite) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Email", email);
        values.put("motDepasse",password);
        values.put("nom",nom);
        values.put("prenom",prenom);
        values.put("dateNaiss", String.valueOf(dateNaiss));
        values.put("nationalite",nationalite);
        values.put("role", UserRole.CANDIDAT.getValue()); // Utiliser la valeur associée à l'énumération UserRole.CANDIDAT
        db.insert("Candidat", null, values);
        db.close();
    }

    public void addCompany(String Email, String password, String name, String departementName,
                           String subDepartementName, String companyNationalId,String contact1Name,String contact2Name,int numero,List<Offre> offres) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Email", Email);
        values.put("motDepasse", password);
        values.put("name",name);
        values.put("departementName",departementName);
        values.put("subDepartementName", subDepartementName);
        values.put("companyNationalId",companyNationalId);
        values.put("contact1Name",contact1Name);
        values.put("contact2Name",contact2Name);
        values.put("numero",numero);
        values.put("role", UserRole.ENTREPRISE.getValue());
        // Convertir la liste d'offres en une représentation textuelle
        String offresJson = null;
        try {
            offresJson = convertOffresToJson(offres);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        values.put("offres", offresJson);

        db.insert("Company", null, values);
        db.close();
    }

    public void addAgence(String Email, String password, String name, String departementName,
                           String subDepartementName, String companyNationalId,String contact1Name,String contact2Name,int numero,List<Offre> offres) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Email", Email);
        values.put("motDepasse", password);
        values.put("Agencename",name);
        values.put("departementName",departementName);
        values.put("subDepartementName", subDepartementName);
        values.put("companyNationalId",companyNationalId);
        values.put("contact1Name",contact1Name);
        values.put("contact2Name",contact2Name);
        values.put("numero",numero);
        values.put("role", UserRole.AGENCE.getValue());
        // Convertir la liste d'offres en une représentation textuelle
        String offresJson = null;
        try {
            offresJson = convertOffresToJson(offres);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        values.put("offres", offresJson);

        db.insert("Agence", null, values);
        db.close();
    }

    public static String convertOffresToJson(List<Offre> offres) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(offres);
    }

    // Méthode pour vérifier si l'adresse e-mail existe déjà dans la table "candidat"
    public boolean checkIfEmailExists(String Email) {
        // Obtenir une instance de la base de données en mode lecture
        SQLiteDatabase db = this.getReadableDatabase();

        // Effectuer une requête pour rechercher l'adresse e-mail dans la table "candidat"
        Cursor cursor = db.query("Candidat", new String[] {"Email"}, "Email = ?", new String[] {Email}, null, null, null);

        // Vérifier si le curseur contient des résultats
        boolean emailExists = cursor.getCount() > 0;

        // Fermer le curseur et la base de données
        cursor.close();
        db.close();

        // Retourner le résultat de la vérification
        return emailExists;
    }

    public boolean checkIfEmailExistsCompany(String Email) {
            // Obtenir une instance de la base de données en mode lecture
            SQLiteDatabase db = this.getReadableDatabase();

            // Effectuer une requête pour rechercher l'adresse e-mail dans la table "candidat"
            Cursor cursor = db.query("Company", new String[] {"Email"}, "Email = ?", new String[] {Email}, null, null, null);

            // Vérifier si le curseur contient des résultats
            boolean emailExists = cursor.getCount() > 0;

            // Fermer le curseur et la base de données
            cursor.close();
            db.close();

            // Retourner le résultat de la vérification
            return emailExists;
    }

    public boolean checkIfEmailExistsAgence(String Email) {
        // Obtenir une instance de la base de données en mode lecture
        SQLiteDatabase db = this.getReadableDatabase();

        // Effectuer une requête pour rechercher l'adresse e-mail dans la table "candidat"
        Cursor cursor = db.query("Agence", new String[] {"Email"}, "Email = ?", new String[] {Email}, null, null, null);

        // Vérifier si le curseur contient des résultats
        boolean emailExists = cursor.getCount() > 0;

        // Fermer le curseur et la base de données
        cursor.close();
        db.close();

        // Retourner le résultat de la vérification
        return emailExists;
    }

    public void saveCandidature(Context context, String nom, String prenom, Date dateNaissance, String nationalite, byte[] cvData, byte[] lettreMotivationData) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("nom", nom);
        values.put("prenom", prenom);
        values.put("dateNaissance", dateNaissance.toString());
        values.put("nationalite", nationalite);
        values.put("cv", cvData);
        values.put("lettreMotivation", lettreMotivationData);
        db.insert("Candidature", null, values);
        db.close();


    }


    public void ajouto(String email, Offre offre) throws JsonProcessingException {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("offres", convertOffresToJson(Arrays.asList(offre))); // Convertir l'objet Offre en une représentation JSON

        String whereClause = "Email = ?";
        String[] whereArgs = {email};

        int numRowsAffected = db.update("Company", values, whereClause, whereArgs);

        db.close();
    }

    public void ajoutoA(String email, Offre offre) throws JsonProcessingException {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("offres", convertOffresToJson(Arrays.asList(offre))); // Convertir l'objet Offre en une représentation JSON

        String whereClause = "Email = ?";
        String[] whereArgs = {email};

        int numRowsAffected = db.update("Agence", values, whereClause, whereArgs);

        db.close();
    }

}

