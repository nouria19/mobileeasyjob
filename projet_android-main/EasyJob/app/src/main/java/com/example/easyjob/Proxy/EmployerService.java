package com.example.easyjob.Proxy;


import com.example.easyjob.Entity.Company;

import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface EmployerService {
    @GET("Employer")
    Call<List<Company>> getAllCompanies();

    @GET("Employer/{id}")
    Call<Company> getCompanyById(@Path("id") Long id);

    @POST("Employer")
    Call<Company> createCompany(@Body Company company);

    @PUT("Employer/{id}")
    Call<Company> updateCompany(@Path("id") Long id, @Body Company company);

    @DELETE("Employer/{id}")
    Call<Void> deleteCompany(@Path("id") Long id);
}
