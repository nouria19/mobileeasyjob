package com.example.easyjob.ModelsAnonyme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.easyjob.Entity.Agence;
import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Entity.Company;
import com.example.easyjob.Entity.DatabaseHelper;
import com.example.easyjob.Entity.Offre;
import com.example.easyjob.ModelsCandidat.AcceuilCActivity;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;

import java.io.IOException;
import java.io.InputStream;

import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class OTPVerification extends AppCompatActivity {

    private EditText otpEt1,otpEt2,otpEt3,otpEt4;
    private TextView resendBtn;
    private Context context;

    private boolean resendEnabled = false;

    private int resendTime = 60;
    private int selectedETPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverification);

        SessionManager sessionManager = SessionManager.getInstance();
        sessionManager.setContext(getApplicationContext());


        context=this;

        otpEt1 = findViewById(R.id.otpET1);
        otpEt2 = findViewById(R.id.otpET2);
        otpEt3 = findViewById(R.id.otpET3);
        otpEt4 = findViewById(R.id.otpET4);

        resendBtn = findViewById(R.id.resendBtn);
        final Button verifyBtn = findViewById(R.id.verifyBtn);
        final TextView otpEmail = findViewById(R.id.otpEmail);
        String getRole = getIntent().getStringExtra("Role");
        if (getRole.equals("candidat")) {
            //récuperer l'email à partir de createcountActivity à travers intent pour candidat
            final String getEmail = getIntent().getStringExtra("Email");
            final String getpassword = getIntent().getStringExtra("password");
            final String getnom = getIntent().getStringExtra("nom");
            final String getprenom = getIntent().getStringExtra("prenom");
            final String getdateNaissString = getIntent().getStringExtra("dateNaissString");
            final String getnationalite = getIntent().getStringExtra("nationalite");


            // Générer un code à 4 chiffres aléatoire
            SecureRandom secureRandom = new SecureRandom();
            int code = secureRandom.nextInt(9000) + 1000; // Générer un nombre aléatoire entre 1000 et 9999 (inclus)
            String verificationCode = String.valueOf(code);

// Vérifier la longueur de la chaîne de caractères et régénérer si nécessaire
            while (verificationCode.length() != 4) {
                code = secureRandom.nextInt(9000) + 1000;
                verificationCode = String.valueOf(code);
            }

// Appeler la fonction sendEmail pour envoyer l'e-mail de vérification
            sendEmail(getEmail, verificationCode);

            //envoyer le mail
            otpEmail.setText(getEmail);

            otpEt1.addTextChangedListener(textWatcher);
            otpEt2.addTextChangedListener(textWatcher);
            otpEt3.addTextChangedListener(textWatcher);
            otpEt4.addTextChangedListener(textWatcher);

            //ouvrir le clavier par défaut sur otpEt1
            showKeyboard(otpEt1);

            //commencer le compte à rebourt
            startCountDownTimer();

            resendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(resendEnabled){
                        startCountDownTimer();

                        // sendEmail(getEmail, verificationCode);
                    }
                }
            });
            String finalVerificationCode = verificationCode;

            verifyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String generateOtp = otpEt1.getText().toString() + otpEt2.getText().toString() + otpEt3.getText().toString() + otpEt4.getText().toString();
                    if (generateOtp.length() == 4) {
                        // Comparer le code saisi et le code généré aléatoirement
                        if (generateOtp.equals(finalVerificationCode)) {
                            // Rediriger vers l'activité "Acceuil" et stocker les infos dans la BD
                            DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());

                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                            Date date = null;
                            try {
                                date = format.parse(getdateNaissString); // Convertir la chaîne de caractères en objet Date
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            Candidat candidat=new Candidat(getnom,getprenom,getnationalite,date,getEmail,getpassword);
                            if (date != null) {
                                dbHelper.addUser(getEmail, getpassword, getnom, getprenom, date, getnationalite);
                                sessionManager.setCurrentUser(candidat);

                            }
                            Intent intent = new Intent(OTPVerification.this, AcceuilCActivity.class);
                            intent.putExtra("Role", getRole);
                            startActivity(intent);
                        } else {
                            // Afficher un message d'erreur
                            Toast.makeText(OTPVerification.this, "Code de vérification incorrect.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            });
        } else if (getRole.equals("entreprise")) {
            //récuperer l'email à partir de createcountActivity à travers intent pour candidat

            final String getEmail = getIntent().getStringExtra("Email");
            final String getpassword = getIntent().getStringExtra("password");
            final String getname = getIntent().getStringExtra("CompanyName");
            final String getdepartementName = getIntent().getStringExtra("departementName");
            final String getsubDepartementName = getIntent().getStringExtra("subDepartementName");
            final String getcompanyNationalId = getIntent().getStringExtra("companyNationalId");
            final String getcontact1Name = getIntent().getStringExtra("contact1Name");
            final String getcontact2Name = getIntent().getStringExtra("contact2Name");
            final int getNumero = getIntent().getIntExtra("numero", 0);


            // Générer un code à 4 chiffres aléatoire
            SecureRandom secureRandom = new SecureRandom();
            int code = secureRandom.nextInt(9000) + 1000; // Générer un nombre aléatoire entre 1000 et 9999 (inclus)
            String verificationCode = String.valueOf(code);

// Vérifier la longueur de la chaîne de caractères et régénérer si nécessaire
            while (verificationCode.length() != 4) {
                code = secureRandom.nextInt(9000) + 1000;
                verificationCode = String.valueOf(code);
            }

// Appeler la fonction sendEmail pour envoyer l'e-mail de vérification
            sendEmail(getEmail, verificationCode);

            //envoyer le mail
            otpEmail.setText(getEmail);

            otpEt1.addTextChangedListener(textWatcher);
            otpEt2.addTextChangedListener(textWatcher);
            otpEt3.addTextChangedListener(textWatcher);
            otpEt4.addTextChangedListener(textWatcher);

            //ouvrir le clavier par défaut sur otpEt1
            showKeyboard(otpEt1);

            //commencer le compte à rebourt
            startCountDownTimer();

            resendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(resendEnabled){
                        startCountDownTimer();

                        // sendEmail(getEmail, verificationCode);
                    }
                }
            });
            String finalVerificationCode = verificationCode;

            verifyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String generateOtp = otpEt1.getText().toString() + otpEt2.getText().toString() + otpEt3.getText().toString() + otpEt4.getText().toString();
                    if (generateOtp.length() == 4) {
                        // Comparer le code saisi et le code généré aléatoirement
                        if (generateOtp.equals(finalVerificationCode)) {
                            // Rediriger vers l'activité "Acceuil" et stocker les infos dans la BD
                            DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());
                            List<Offre> offres = new ArrayList<Offre>();

                            dbHelper.addCompany(getEmail, getpassword, getname, getdepartementName, getsubDepartementName, getcompanyNationalId,getcontact1Name,getcontact2Name,getNumero,offres);
                            Company company =new Company(getEmail, getpassword, getname, getdepartementName, getsubDepartementName, getcompanyNationalId,getcontact1Name,getcontact2Name,getNumero);
                            sessionManager.setCurrentCompany(company);

                            }
                            Intent intent = new Intent(OTPVerification.this, AcceuilCActivity.class);
                            intent.putExtra("Role", getRole);
                            startActivity(intent);
                        } else {
                            // Afficher un message d'erreur
                            Toast.makeText(OTPVerification.this, "Code de vérification incorrect.", Toast.LENGTH_SHORT).show();
                        }
                    }
                

            });

        }else if (getRole.equals("agence")) {
            //récuperer l'email à partir de createcountActivity à travers intent pour candidat

            final String getEmail = getIntent().getStringExtra("Email");
            final String getpassword = getIntent().getStringExtra("password");
            final String getname = getIntent().getStringExtra("Agencename");
            final String getdepartementName = getIntent().getStringExtra("departementName");
            final String getsubDepartementName = getIntent().getStringExtra("subDepartementName");
            final String getcompanyNationalId = getIntent().getStringExtra("companyNationalId");
            final String getcontact1Name = getIntent().getStringExtra("contact1Name");
            final String getcontact2Name = getIntent().getStringExtra("contact2Name");
            final int getNumero = getIntent().getIntExtra("numero", 0);


            // Générer un code à 4 chiffres aléatoire
            SecureRandom secureRandom = new SecureRandom();
            int code = secureRandom.nextInt(9000) + 1000; // Générer un nombre aléatoire entre 1000 et 9999 (inclus)
            String verificationCode = String.valueOf(code);

// Vérifier la longueur de la chaîne de caractères et régénérer si nécessaire
            while (verificationCode.length() != 4) {
                code = secureRandom.nextInt(9000) + 1000;
                verificationCode = String.valueOf(code);
            }

// Appeler la fonction sendEmail pour envoyer l'e-mail de vérification
            sendEmail(getEmail, verificationCode);

            //envoyer le mail
            otpEmail.setText(getEmail);

            otpEt1.addTextChangedListener(textWatcher);
            otpEt2.addTextChangedListener(textWatcher);
            otpEt3.addTextChangedListener(textWatcher);
            otpEt4.addTextChangedListener(textWatcher);

            //ouvrir le clavier par défaut sur otpEt1
            showKeyboard(otpEt1);

            //commencer le compte à rebourt
            startCountDownTimer();

            resendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(resendEnabled){
                        startCountDownTimer();

                        // sendEmail(getEmail, verificationCode);
                    }
                }
            });
            String finalVerificationCode = verificationCode;

            verifyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String generateOtp = otpEt1.getText().toString() + otpEt2.getText().toString() + otpEt3.getText().toString() + otpEt4.getText().toString();
                    if (generateOtp.length() == 4) {
                        // Comparer le code saisi et le code généré aléatoirement
                        if (generateOtp.equals(finalVerificationCode)) {
                            // Rediriger vers l'activité "Acceuil" et stocker les infos dans la BD
                            DatabaseHelper dbHelper = new DatabaseHelper(getApplicationContext());
                            List<Offre> offres = new ArrayList<Offre>();

                            dbHelper.addAgence(getEmail, getpassword, getname, getdepartementName, getsubDepartementName, getcompanyNationalId,getcontact1Name,getcontact2Name,getNumero,offres);
                            Agence agence =new Agence(getEmail, getpassword, getname, getdepartementName, getsubDepartementName, getcompanyNationalId,getcontact1Name,getcontact2Name,getNumero);
                            sessionManager.setCurrentAgence(agence);

                        }
                        Intent intent = new Intent(OTPVerification.this, AcceuilCActivity.class);
                        intent.putExtra("Role", getRole);
                        startActivity(intent);
                    } else {
                        // Afficher un message d'erreur
                        Toast.makeText(OTPVerification.this, "Code de vérification incorrect.", Toast.LENGTH_SHORT).show();
                    }
                }


            });

        }



    }

    private void sendEmail(String email, String otp) {
    // Charger les propriétés à partir du fichier de propriétés
        /*Properties props1 = new Properties();
            try {
            InputStream inputStream = context.getResources().getAssets().open("gradle.properties");
            props1.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }*/


    // Obtenir les valeurs des propriétés
    String username = "easyjobdjn2023@gmail.com";
    String password = "wisspdmuskyrunvl";

        Properties props = new Properties();
        // Paramètres SMTP
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "465");

        // Créer une session de messagerie avec l'authentification
        javax.mail.Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            // Créer un objet MimeMessage pour composer le message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            message.setSubject("Code de vérification OTP");
            message.setText("Votre code de vérification OTP est : " + otp);

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    // Envoyer le message
                    try {
                        Transport.send(message);
                    } catch (MessagingException e) {
                        throw new RuntimeException(e);
                    }

                }
            });

            thread.start();

            // Afficher un message de succès
            Toast.makeText(this, "E-mail de vérification envoyé avec succès.", Toast.LENGTH_SHORT).show();

        } catch (MessagingException e) {
            // Afficher un message d'erreur
            Toast.makeText(this, "Erreur lors de l'envoi de l'e-mail de vérification.", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    private void showKeyboard(EditText otpET){
        otpET.requestFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(otpET,InputMethodManager.SHOW_IMPLICIT);
    }

    private void startCountDownTimer(){
        resendEnabled=false;
        resendBtn.setTextColor(Color.parseColor("#99000000"));

        new CountDownTimer(resendTime * 1000, 1000){

            @Override
            public void onTick(long l) {
                resendBtn.setText("Réenvoyer le code("+(l / 1000)+")");
            }

            @Override
            public void onFinish() {
                resendEnabled=true;
                resendBtn.setText("Réenvoyer le code");
                resendBtn.setTextColor(getResources().getColor(R.color.blueF));
            }
        }.start();
    }

    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if(editable.length() > 0){
                if(selectedETPosition == 0){
                    selectedETPosition=1;
                    showKeyboard(otpEt2);

                }else if(selectedETPosition == 1){
                    selectedETPosition=2;
                    showKeyboard(otpEt3);

                }else if(selectedETPosition == 2){
                    selectedETPosition=3;
                    showKeyboard(otpEt4);

                }
            }

        }
    };
    
    @Override
    public boolean onKeyUp(int KeyCode, KeyEvent event){
       if(KeyCode == KeyEvent.KEYCODE_DEL){
           if(selectedETPosition == 3){
               selectedETPosition = 2;
               showKeyboard(otpEt3);
           }else if(selectedETPosition == 2){
               selectedETPosition=1;
               showKeyboard(otpEt2);
           }else if(selectedETPosition==1){
               selectedETPosition=0;
               showKeyboard(otpEt1);
           }
           return true;
           
       }
       else{
           return super.onKeyUp(KeyCode,event);
       }

    }
}