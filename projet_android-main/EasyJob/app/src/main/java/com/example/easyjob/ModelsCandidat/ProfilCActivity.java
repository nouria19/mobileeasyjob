package com.example.easyjob.ModelsCandidat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;

import java.util.Date;

public class ProfilCActivity extends AppCompatActivity {

    private TextView textViewNationalite;
    private TextView textViewDateNaiss;
    private TextView textViewTel;
    private TextView textView2; // nom+prenom
    private TextView textView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_cactivity);

        SessionManager sessionManager = SessionManager.getInstance();
        sessionManager.setContext(getApplicationContext());

        // Récupérer les références des vues
        textViewNationalite = findViewById(R.id.nationalityTextView);
        textViewDateNaiss = findViewById(R.id.dobTextView);
        textViewTel = findViewById(R.id.phoneTextView);
        textView2 = findViewById(R.id.nameTextView);
        textView3 = findViewById(R.id.emailTextView);

        if (sessionManager.isLoggedIn()) {
            Candidat utilisateur = sessionManager.getCurrentUser();

            // Récupérer les informations de l'utilisateur
            String nom = utilisateur.getNom();
            String prenom = utilisateur.getPrenom();
            String email = utilisateur.getEmail();
            String nationalite = utilisateur.getNationalite();
            Date dateNaiss = utilisateur.getDateNaiss();


            // Mettre à jour les vues avec les informations de l'utilisateur
            TextView textViewNomPrenom = findViewById(R.id.nameTextView);
            TextView textViewEmail = findViewById(R.id.emailTextView);
            TextView textViewNationalite = findViewById(R.id.nationalityTextView);
            TextView textViewDateNaiss = findViewById(R.id.dobTextView);
            TextView textViewTel = findViewById(R.id.phoneTextView);

            textViewNomPrenom.setText(nom + " " + prenom);
            textViewEmail.setText(email);
            textViewNationalite.setText("Nationality: " + nationalite);
            textViewDateNaiss.setText("Date of Birth: " + dateNaiss);

        }

    }
}

