package com.example.easyjob.ModelsCandidat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Entity.DatabaseHelper;
import com.example.easyjob.Entity.UserRole;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executors;

public class CandidatureActivity extends AppCompatActivity {

    private EditText nomEditText;
    private EditText prenomEditText;
    private EditText nationaliteEditText;

    private EditText dateBirthEditText;
    private EditText cvFilePathEditText;
    private EditText lettreMotivationFilePathEditText;

    private Button selectCvButton;
    private Button selectLettreMotivationButton;
    private Button submitButton;

    private byte[] cvData;
    private byte[] lettreMotivationData;

    private static final int REQUEST_CODE_SELECT_CV = 1;
    private static final int REQUEST_CODE_SELECT_LETTRE_MOTIVATION = 2;
    SessionManager sessionManager;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidature);

        // Instancier DatabaseHelper
        databaseHelper = new DatabaseHelper(this);

        sessionManager = SessionManager.getInstance();
        sessionManager.setContext(getApplicationContext());

        nomEditText = findViewById(R.id.editTextNom);
        prenomEditText = findViewById(R.id.editTextPrenom);
        nationaliteEditText = findViewById(R.id.editTextNationalite);
        dateBirthEditText = findViewById(R.id.dateBirthEditText);
        cvFilePathEditText = findViewById(R.id.editTextCvFilePath);
        lettreMotivationFilePathEditText = findViewById(R.id.editTextLettreMotivationFilePath);

        selectCvButton = findViewById(R.id.buttonSelectCv);
        selectLettreMotivationButton = findViewById(R.id.buttonSelectLettreMotivation);
        submitButton = findViewById(R.id.buttonSubmit);
        populateFieldsFromSession();

        selectCvButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Ouvrir une activité de sélection de fichier PDF pour le CV
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                startActivityForResult(intent, REQUEST_CODE_SELECT_CV);
            }
        });

        selectLettreMotivationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Ouvrir une activité de sélection de fichier PDF pour la lettre de motivation
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                startActivityForResult(intent, REQUEST_CODE_SELECT_LETTRE_MOTIVATION);
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Récupérer les données saisies
                String nom = nomEditText.getText().toString();
                String prenom = prenomEditText.getText().toString();
                String nationalite = nationaliteEditText.getText().toString();
                String dateNaissanceString = dateBirthEditText.getText().toString();

                // Convertir la date de naissance en objet Date
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-DD", Locale.getDefault());
                Date dateNaissance = null;
                try {
                    dateNaissance = dateFormat.parse(dateNaissanceString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // Récupérer les données du fichier CV
                String cvFilePath = cvFilePathEditText.getText().toString();
                byte[] cvData = null;
                if (!TextUtils.isEmpty(cvFilePath)) {
                    try {
                        File cvFile = new File(cvFilePath);
                        FileInputStream cvFileInputStream = new FileInputStream(cvFile);
                        cvData = new byte[(int) cvFile.length()];
                        cvFileInputStream.read(cvData);
                        cvFileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                // Récupérer les données du fichier de lettre de motivation
                String lettreMotivationFilePath = lettreMotivationFilePathEditText.getText().toString();
                byte[] lettreMotivationData = null;
                if (!TextUtils.isEmpty(lettreMotivationFilePath)) {
                    try {
                        File lettreMotivationFile = new File(lettreMotivationFilePath);
                        FileInputStream lettreMotivationFileInputStream = new FileInputStream(lettreMotivationFile);
                        lettreMotivationData = new byte[(int) lettreMotivationFile.length()];
                        lettreMotivationFileInputStream.read(lettreMotivationData);
                        lettreMotivationFileInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                databaseHelper.saveCandidature(CandidatureActivity.this, nom, prenom, dateNaissance, nationalite, cvData, lettreMotivationData);
                // Afficher un message de confirmation ou rediriger vers une autre activité
                // Afficher un message de confirmation ou rediriger vers une autre activité

                Intent intent = new Intent(CandidatureActivity.this, AcceuilCActivity.class);
                intent.putExtra("Role", UserRole.CANDIDAT.getValue());
                startActivity(intent);
                // Envoyer une notification
                sendNotification( CandidatureActivity.this,"Candidature envoyée","Votre candidature a été enregistrée avec succès!");

            }
            // Méthode pour envoyer une notification
            private void sendNotification(Context context, String title, String content) {
                // Créer un intent pour envoyer un Broadcast
                Intent intent = new Intent("com.example.easyjob.ModelsCandidat.ACTION_NOTIFICATION");
                intent.putExtra("title", title);
                intent.putExtra("content", content);
                // Utiliser le contexte de l'activité pour envoyer la diffusion
                context.sendBroadcast(intent);


            }

        });

    }



    private void populateFieldsFromSession() {

        if (sessionManager.isLoggedIn()) {
            Candidat utilisateur = sessionManager.getCurrentUser();

            // Pré-remplir les champs avec les données de l'utilisateur
            nomEditText.setText(utilisateur.getNom());
            prenomEditText.setText(utilisateur.getPrenom());
            nationaliteEditText.setText(utilisateur.getNationalite());
            // Autres champs à pré-remplir

            // Convertir la date en chaîne de caractères dans le format souhaité
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date dateNaiss = utilisateur.getDateNaiss();
            if (dateNaiss != null) {
                String dateString = dateFormat.format(dateNaiss);
                dateBirthEditText.setText(dateString);
            } else {
                // Gérer le cas où la date est nulle
                dateBirthEditText.setText("");
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_SELECT_CV || requestCode == REQUEST_CODE_SELECT_LETTRE_MOTIVATION) {
                if (data != null && data.getData() != null) {
                    // Récupérer le fichier PDF sélectionné
                    InputStream inputStream = null;
                    try {
                        inputStream = getContentResolver().openInputStream(data.getData());
                        byte[] fileData = convertInputStreamToByteArray(inputStream);

                        if (requestCode == REQUEST_CODE_SELECT_CV) {
                            // Stocker les données du CV
                            cvData = fileData;
                            cvFilePathEditText.setText(data.getData().getPath());
                        } else if (requestCode == REQUEST_CODE_SELECT_LETTRE_MOTIVATION) {
                            // Stocker les données de la lettre de motivation
                            lettreMotivationData = fileData;
                            lettreMotivationFilePathEditText.setText(data.getData().getPath());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    }

    private byte[] convertInputStreamToByteArray(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, bytesRead);
        }
        return byteBuffer.toByteArray();
    }

}
