package com.example.easyjob.Proxy;

import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Entity.Subscription;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;



public interface CandidatService {
    @GET("candidates")
    Call<List<Candidat>> getAllCandidates();

    @GET("candidates/{id}")
    Call<Candidat> getCandidateById(@Path("id") Long id);

    @POST("candidates")
    Call<Void> addCandidate(@Body Candidat candidate);

    @PUT("candidates/{id}")
    Call<Void> updateCandidate(@Path("id") Long id, @Body Candidat candidate);

    @DELETE("candidates/{id}")
    Call<Void> deleteCandidate(@Path("id") Long id);
}

