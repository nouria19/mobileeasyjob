package com.example.easyjob.Entity;

public enum UserRole {
        CANDIDAT("candidat"),
        ENTREPRISE("entreprise"),
        AGENCE("agence"),
        GESTIONNAIRE("gestionnaire")
        ;

        private String value;

        UserRole(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }


