package com.example.easyjob.ModelsCandidat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.easyjob.R;

public class NotificationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        // Récupérer les données de la notification
        String title = intent.getStringExtra("title");
        String content = intent.getStringExtra("content");
        Toast.makeText(context, "Notification received: " + title + " - " + content, Toast.LENGTH_SHORT).show();

        // Récupérer une référence au TextView dans l'activité AcceuilCActivity
        CandidatureActivity activity = (CandidatureActivity) context;
        TextView notificationTextView = activity.findViewById(R.id.noti);

        // Afficher les données dans le TextView
        notificationTextView.setText(title + ": " + content);
    }
}
