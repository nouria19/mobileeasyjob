package com.example.easyjob.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;


public class Candidature implements Parcelable {

        private int id;
    private String nom;
    private String prenom;
    private Date dateNaissance;
    private String nationalite;
    private byte[] cv;
    private byte[] lettreMotivation;

    // Constructeur
    public Candidature(String nom, String prenom, Date dateNaissance, String nationalite, byte[] cv, byte[] lettreMotivation) {
        this.nom = nom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.nationalite = nationalite;
        this.cv = cv;
        this.lettreMotivation = lettreMotivation;
    }

    // Getters et setters

    private Candidature(Parcel in) {
        id = in.readInt();
        nom = in.readString();
        prenom = in.readString();
        nationalite = in.readString();
        cv = in.createByteArray();
        lettreMotivation = in.createByteArray();
    }


    public static final Creator<Candidature> CREATOR = new Creator<Candidature>() {
        @Override
        public Candidature createFromParcel(Parcel in) {
            return new Candidature(in);
        }

        @Override
        public Candidature[] newArray(int size) {
            return new Candidature[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public byte[] getCv() {
        return cv;
    }

    public void setCv(byte[] cv) {
        this.cv = cv;
    }

    public byte[] getLettreMotivation() {
        return lettreMotivation;
    }

    public void setLettreMotivation(byte[] lettreMotivation) {
        this.lettreMotivation = lettreMotivation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(nom);
        parcel.writeString(prenom);
        parcel.writeString(nationalite);
        parcel.writeByteArray(cv);
        parcel.writeByteArray(lettreMotivation);
    }
}

