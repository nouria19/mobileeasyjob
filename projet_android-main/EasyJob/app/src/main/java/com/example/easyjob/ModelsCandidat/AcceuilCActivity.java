package com.example.easyjob.ModelsCandidat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.easyjob.Entity.Agence;
import com.example.easyjob.Entity.Candidat;
import com.example.easyjob.Entity.Candidature;
import com.example.easyjob.Entity.Company;
import com.example.easyjob.Entity.DatabaseHelper;
import com.example.easyjob.Entity.Offre;
import com.example.easyjob.Entity.OffreAdapter;
import com.example.easyjob.ModelsAgence.ProfilaActivity;
import com.example.easyjob.ModelsAnonyme.LoginActivity;
import com.example.easyjob.ModelsEmployeur.AjoutOffreActivity;
import com.example.easyjob.ModelsEmployeur.MesOffresActivity;
import com.example.easyjob.ModelsEmployeur.ProfilEActivity;
import com.example.easyjob.Proxy.SessionManager;
import com.example.easyjob.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AcceuilCActivity extends AppCompatActivity {

    BottomNavigationView nav;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ActionBarDrawerToggle drawerToggle;

    private SearchView searchView;

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private OffreAdapter offreAdapter;

    private List<Offre> allOffres = new ArrayList<>();
    private SQLiteDatabase db;
    Context context;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        if(drawerToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acceuil_cactivity);



        SessionManager sessionManager = SessionManager.getInstance();
        sessionManager.setContext(getApplicationContext());
        sessionManager.setLoggedIn(true);


        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);
        TextView nameTextView = headerView.findViewById(R.id.textViewName);

        // Récupération de l'intent qui a lancé cette activité
        Intent intent = getIntent();
        String getRole = intent.getStringExtra("Role");

        if (sessionManager.isLoggedIn()) {
            if (getRole.equals("candidat")) {
                Candidat utilisateur = sessionManager.getCurrentUser();
                String nomPrenom = utilisateur.getNom() + " " + utilisateur.getPrenom();
                nameTextView.setText(nomPrenom);

                // Obtenir une référence au HeaderView et mettre à jour le TextView avec les informations de l'utilisateur connecté


                this.context = AcceuilCActivity.this;
                recyclerView = findViewById(R.id.recycler_view);


                progressBar = findViewById(R.id.progressBar);

                progressBar.setVisibility(View.GONE);

                // Instancier la base de données
                DatabaseHelper dbHelper = new DatabaseHelper(this);
                db = dbHelper.getReadableDatabase();

                // Récupérer toutes les offres et les stocker dans une liste
                allOffres = setupRecyclerView();

                // Configurer le RecyclerView avec l'adapter et le layout manage
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                offreAdapter = new OffreAdapter(allOffres, context);
                recyclerView.setAdapter(offreAdapter);

            } else if (getRole.equals("entreprise")) {
                Company com = sessionManager.getCurrentCompany();
                String nomCompany = com.getName();
                nameTextView.setText(nomCompany);

                Log.d("DEBUG", "Le rôle de l'utilisateur est 'entreprise'");

                recyclerView = findViewById(R.id.recycler_view);
                progressBar = findViewById(R.id.progressBar);
                progressBar.setVisibility(View.GONE);

                ImageView ajout = findViewById(R.id.ajoutbutton);
                ajout.setVisibility(View.VISIBLE);
                Log.d("DEBUG", "Le bouton d'ajout est rendu visible");

                ajout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(AcceuilCActivity.this, AjoutOffreActivity.class);
                        intent.putExtra("Role", getRole);
                        startActivity(intent);
                    }
                });

                // Instancier la base de données
                DatabaseHelper dbHelper = new DatabaseHelper(this);
                db = dbHelper.getReadableDatabase();

                allOffres = setupRecyclerView2();

                if (allOffres.isEmpty()) {
                    ImageView emptyImageView = findViewById(R.id.empty_image_view);
                    emptyImageView.setVisibility(View.VISIBLE);
                } else {
                    // Configurer le RecyclerView avec l'adapter et le layout manager
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    offreAdapter = new OffreAdapter(allOffres, context);
                    recyclerView.setAdapter(offreAdapter);
                }
            }else if(getRole.equals("agence")){
                Agence agence = sessionManager.getCurrentAgence();
                String nomAgence = agence.getName();
                nameTextView.setText(nomAgence);
                Log.d("DEBUG", "ici agence " + nomAgence);

                Log.d("DEBUG", "Le rôle de l'utilisateur est 'agence'");

                recyclerView = findViewById(R.id.recycler_view);
                progressBar = findViewById(R.id.progressBar);
                progressBar.setVisibility(View.GONE);

                ImageView ajout = findViewById(R.id.ajoutbutton);
                ajout.setVisibility(View.VISIBLE);
                Log.d("DEBUG", "Le bouton d'ajout est rendu visible");

                ajout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(AcceuilCActivity.this, AjoutOffreActivity.class);
                        intent.putExtra("Role", getRole);
                        startActivity(intent);
                    }
                });

                // Instancier la base de données
                DatabaseHelper dbHelper = new DatabaseHelper(this);
                db = dbHelper.getReadableDatabase();

                allOffres = setupRecyclerView3();

                if (allOffres.isEmpty()) {
                    ImageView emptyImageView = findViewById(R.id.empty_image_view);
                    emptyImageView.setVisibility(View.VISIBLE);
                } else {
                    // Configurer le RecyclerView avec l'adapter et le layout manager
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    offreAdapter = new OffreAdapter(allOffres, context);
                    recyclerView.setAdapter(offreAdapter);
                }
            }
        }


        // Enregistrez le BroadcastReceiver pour les notifications
        NotificationReceiver receiver = new NotificationReceiver();
        IntentFilter intentFilter = new IntentFilter("com.example.easyjob.ModelsCandidat.ACTION_NOTIFICATION");
        registerReceiver(receiver, intentFilter);

        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.Open,R.string.Close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.home: {
                        Toast.makeText(AcceuilCActivity.this, "Home Selected", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AcceuilCActivity.this, AcceuilCActivity.class);
                        intent.putExtra("Role", getRole);
                        startActivity(intent);
                        break;
                    }

                    case R.id.gallery: {
                        Toast.makeText(AcceuilCActivity.this, "Applications Selected", Toast.LENGTH_SHORT).show();
                        if(getRole.equals("entreprise") || getRole.equals("agence")) {
                            Intent intent = new Intent(AcceuilCActivity.this, MesOffresActivity.class);
                            startActivity(intent);
                            break;
                        }else{
                            Intent intent = new Intent(AcceuilCActivity.this, CandidaturesAfficheActivity.class);
                            startActivity(intent);
                            break;
                        }

                    }

                    case R.id.noti: {
                        Toast.makeText(AcceuilCActivity.this, "Notification Selected", Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case R.id.job: {
                        Toast.makeText(AcceuilCActivity.this, "Jobs Selected", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("content://com.android.calendar/time"));
                        startActivity(intent);
                        break;
                    }

                    case R.id.about:
                    {
                        Toast.makeText(AcceuilCActivity.this, "About Selected", Toast.LENGTH_SHORT).show();
                        if(getRole.equals("candidat")){
                            Intent intent = new Intent(AcceuilCActivity.this, ProfilCActivity.class);
                            intent.putExtra("Role", getRole);
                            startActivity(intent);
                            break;
                        }else if(getRole.equals("entreprise")){
                            Intent intent = new Intent(AcceuilCActivity.this, ProfilEActivity.class);
                            intent.putExtra("Role", getRole);
                            startActivity(intent);
                            break;
                        }else if(getRole.equals(("agence"))){
                            Intent intent = new Intent(AcceuilCActivity.this, ProfilaActivity.class);
                            intent.putExtra("Role", getRole);
                            startActivity(intent);
                            break;
                        }

                }

                    case R.id.Login:
                    {
                        Toast.makeText(AcceuilCActivity.this, "Log in Selected", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AcceuilCActivity.this, LoginActivity.class);
                        sessionManager.setLoggedIn(false);
                        startActivity(intent);
                        break;
                    }

                    case R.id.share:
                    {
                        Toast.makeText(AcceuilCActivity.this, "Share Selected", Toast.LENGTH_SHORT).show();
                        String deepLinkUrl = createDeepLinkUrl();
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_TEXT, deepLinkUrl);
                        startActivity(Intent.createChooser(shareIntent, "Share activity URL"));
                        break;
                    }

                    case R.id.rate_us:
                    {
                        Toast.makeText(AcceuilCActivity.this, "Rate Us Selected", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AcceuilCActivity.this, HelpActivity.class);
                        startActivity(intent);
                        break;

                    }
                }
                return true;
            }
        });

        nav = findViewById(R.id.nav);
        nav.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.home:
                        Toast.makeText(AcceuilCActivity.this, "Home", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(AcceuilCActivity.this, AcceuilCActivity.class);
                        intent.putExtra("Role", getRole);
                        sessionManager.setLoggedIn(true);
                        startActivity(intent);
                        break;

                    case R.id.recherche://retrofit, c'est le frontend
                        Toast.makeText(AcceuilCActivity.this, "Search", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.notif:
                        Toast.makeText(AcceuilCActivity.this, "Notification", Toast.LENGTH_SHORT).show();
                        break;

                    default:
                }
                return true;
            }
        });

    }

    private String createDeepLinkUrl() {
        String packageName = getPackageName();
        String activityName = AcceuilCActivity.class.getSimpleName();
        String deepLinkUri = "https://easy.com/MyApplication";
        String deepLinkUrl = "android-app://" + packageName + "/" + activityName + "?deeplink=" + deepLinkUri;
        return deepLinkUrl;
    }

    @SuppressLint("Range")
    private List<Offre> setupRecyclerView () {

        // Exécuter la requête SQL pour récupérer les offres
        String query = "SELECT * FROM Offre";
        Cursor cursor = db.rawQuery(query, null);

        // Parcours des résultats de la requête et création d'une liste d'objets Offre
        // on crée la classe Offre pour faciliter la manipulation
        // et le transfert de ces données dans l'application.
        List<Offre> offres = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Offre offre = new Offre();
                offre.setTitreOffre(cursor.getString(cursor.getColumnIndex("titreOffre")));
                offre.setTypeContrat(cursor.getString(cursor.getColumnIndex("typeContrat")));
                offre.setVille(cursor.getString(cursor.getColumnIndex("Ville")));
                offre.setCodePostal(cursor.getInt(cursor.getColumnIndex("CodePostal")));
                offre.setNumero(cursor.getInt(cursor.getColumnIndex("numéro")));
                offre.setSalaire(cursor.getInt(cursor.getColumnIndex("Salaire")));
                offre.setRue(cursor.getString(cursor.getColumnIndex("rue")));
                offre.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                offre.setDateOffre(new Date(cursor.getLong(cursor.getColumnIndex("dateOffre"))));
                offre.setDatePublication(new Timestamp(cursor.getLong(cursor.getColumnIndex("datePublication"))));


                offres.add(offre);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return offres;
    }

    @SuppressLint("Range")
    private List<Offre> setupRecyclerView2() {
        // Exécuter la requête SQL pour récupérer les offres de la table "Company"
        String query = "SELECT offres FROM Company";
        Cursor cursor = db.rawQuery(query, null);

        // Parcours des résultats de la requête et création de la liste d'objets Offre
        List<Offre> offres = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                // Récupérer la représentation textuelle du tableau d'offres
                String offresJson = cursor.getString(cursor.getColumnIndex("offres"));

                // Convertir la chaîne JSON en une liste d'offres
                List<Offre> offresList = convertJsonToOffres(offresJson);

                // Ajouter les offres à la liste principale
                offres.addAll(offresList);
            } while (cursor.moveToNext());
        }

        // Fermer le curseur
        cursor.close();

        // Retourner la liste d'offres
        return offres;
    }

    @SuppressLint("Range")
    private List<Offre> setupRecyclerView3() {
        // Exécuter la requête SQL pour récupérer les offres de la table "Company"
        String query = "SELECT offres FROM Agence";
        Cursor cursor = db.rawQuery(query, null);

        // Parcours des résultats de la requête et création de la liste d'objets Offre
        List<Offre> offres = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                // Récupérer la représentation textuelle du tableau d'offres
                String offresJson = cursor.getString(cursor.getColumnIndex("offres"));

                // Convertir la chaîne JSON en une liste d'offres
                List<Offre> offresList = convertJsonToOffres(offresJson);

                // Ajouter les offres à la liste principale
                offres.addAll(offresList);
            } while (cursor.moveToNext());
        }

        // Fermer le curseur
        cursor.close();

        // Retourner la liste d'offres
        return offres;
    }

    private List<Offre> convertJsonToOffres(String json) {
        List<Offre> offresList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(json);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonOffre = jsonArray.getJSONObject(i);

                // Extraire les attributs de l'objet JSON
                Long id = jsonOffre.getLong("id");
                String titreOffre = jsonOffre.getString("titreOffre");
                String ville = jsonOffre.getString("Ville");
                int codePostal = jsonOffre.getInt("CodePostal");
                // Extraire la date de l'objet JSON en tant que chaîne de caractères
                String dateOffreStr = jsonOffre.getString("dateOffre");
                int Salaire = jsonOffre.getInt("Salaire");
                int numero = jsonOffre.getInt("numéro");
                String rue = jsonOffre.getString("rue");
                String typeContrat = jsonOffre.getString("typeContrat");
                String description = jsonOffre.getString("description");

                String datePublicationStr = jsonOffre.getString("datePublication");

                // Convertir la chaîne de caractères en objet Timestamp
                Timestamp datePublication = null;
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date parsedDate = dateFormat.parse(datePublicationStr);
                    if (parsedDate != null) {
                        datePublication = new Timestamp(parsedDate.getTime());
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                JSONArray jsonCandidatures = jsonOffre.getJSONArray("candidatures");

                // Créer une liste pour stocker les candidatures
                ArrayList<Candidature> candidatures = new ArrayList<>();

                // Parcourir les candidatures JSON
                for (int j = 0; j < jsonCandidatures.length(); j++) {
                    JSONObject jsonCandidature = jsonCandidatures.getJSONObject(j);

                    // Extraire les attributs de l'objet JSON de la candidature
                    // et créer l'objet Candidature correspondant

                    // Exemple d'extraction d'attributs pour la candidature
                    Long candidatureId = jsonCandidature.getLong("id");
                    String candidatureNom = jsonCandidature.getString("nom");
                    String candidaturePrenom = jsonCandidature.getString("prenom");
                    String candidatureDateNaissanceStr = jsonCandidature.getString("dateNaissance");
                    String candidatureNationalite = jsonCandidature.getString("nationalite");
                    // Extraire les attributs des fichiers de candidature (cv et lettre de motivation)
                    byte[] candidatureCV = Base64.decode(jsonCandidature.getString("cv"), Base64.DEFAULT);
                    byte[] candidatureLettreMotivation = Base64.decode(jsonCandidature.getString("lettreMotivation"), Base64.DEFAULT);


                    // Convertir la chaîne de caractères en objet Date pour la date de naissance
                    Date candidatureDateNaissance = null;
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        candidatureDateNaissance = dateFormat.parse(candidatureDateNaissanceStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    // Créer l'objet Candidature correspondant avec les attributs extraits
                    Candidature candidature = new Candidature(candidatureNom, candidaturePrenom, candidatureDateNaissance, candidatureNationalite, candidatureCV, candidatureLettreMotivation);
                    candidatures.add(candidature);
                }

                // Convertir la chaîne de caractères en objet Date
                Date dateOffre = null;
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    dateOffre = dateFormat.parse(dateOffreStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                // Créer un objet Offre avec les attributs extraits
                Offre offre = new Offre();
                offre.setId(id);
                offre.setTitreOffre(titreOffre);
                offre.setVille(ville);
                offre.setCodePostal(codePostal);
                // Définir les autres attributs de l'offre

                // Ajouter l'offre à la liste
                offresList.add(offre);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return offresList;
    }




    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout,fragment);
        fragmentTransaction.commit();
    }
    @Override
    public void onBackPressed(){
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }
}
