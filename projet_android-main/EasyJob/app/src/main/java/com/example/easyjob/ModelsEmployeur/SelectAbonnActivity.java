package com.example.easyjob.ModelsEmployeur;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.easyjob.R;


public class SelectAbonnActivity extends AppCompatActivity {

    Button button1;
    Button button2;
    Button button3;
    Button button4;
    Button button5;
    Button button6;
    String getEmail;
    String getpassword;
    String getname;
    String getdepartementName;
    String getsubDepartementName;
    String getcompanyNationalId;
    String getcontact1Name;
    String getcontact2Name;
    int getnumero;
    String getRole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_abonn);

        button1 = findViewById(R.id.onetime);
        button2 = findViewById(R.id.onemonth);
        button3 = findViewById(R.id.oneterm);
        button4 = findViewById(R.id.onesemestre);
        button5 = findViewById(R.id.oneyear);
        button6 = findViewById(R.id.indefinite);

          getEmail = getIntent().getStringExtra("Email");
        getpassword = getIntent().getStringExtra("password");
        getname = getIntent().getStringExtra("CompanyName");
        getdepartementName = getIntent().getStringExtra("departementName");
        getsubDepartementName = getIntent().getStringExtra("subDepartementName");
        getcompanyNationalId = getIntent().getStringExtra("companyNationalId");
        getcontact1Name = getIntent().getStringExtra("contact1Name");
        getcontact2Name = getIntent().getStringExtra("contact2Name");
        getnumero = getIntent().getIntExtra("numero",0);
        getRole = getIntent().getStringExtra("Role");

        if(getRole.equals("entreprise")){
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails(button1.getText().toString());
                }
            });

            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails(button2.getText().toString());
                }
            });

            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails(button3.getText().toString());
                }
            });

            button4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails(button4.getText().toString());
                }
            });

            button5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails(button5.getText().toString());
                }
            });

            button6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails(button6.getText().toString());
                }
            });

        }else if(getRole.equals("agence")){
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails2(button1.getText().toString());
                }
            });

            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails2(button2.getText().toString());
                }
            });

            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails2(button3.getText().toString());
                }
            });

            button4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails2(button4.getText().toString());
                }
            });

            button5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails2(button5.getText().toString());
                }
            });

            button6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    redirectToSelectionDetails2(button6.getText().toString());
                }
            });

        }


    }

    private void redirectToSelectionDetails(String subscriptionType) {
        Intent intent = new Intent(SelectAbonnActivity.this, SubscribeDetailActivity.class);
        intent.putExtra("type", subscriptionType);
        intent.putExtra("Email",getEmail);
        intent.putExtra("password",getpassword);
        intent.putExtra("Company_Name",getname);
        intent.putExtra("departementName",getdepartementName);
        intent.putExtra("subDepartementName",getsubDepartementName);
        intent.putExtra("companyNationalId",getcompanyNationalId);
        intent.putExtra("contact1Name",getcontact1Name);
        intent.putExtra("contact2Name",getcontact2Name);
        intent.putExtra("numero",getnumero);
        intent.putExtra("Role", getRole);
        startActivity(intent);
    }

    private void redirectToSelectionDetails2(String subscriptionType) {
        Intent intent = new Intent(SelectAbonnActivity.this, SubscribeDetailActivity.class);
        intent.putExtra("type", subscriptionType);
        intent.putExtra("Email",getEmail);
        intent.putExtra("password",getpassword);
        intent.putExtra("Agencename",getname);
        intent.putExtra("departementName",getdepartementName);
        intent.putExtra("subDepartementName",getsubDepartementName);
        intent.putExtra("companyNationalId",getcompanyNationalId);
        intent.putExtra("contact1Name",getcontact1Name);
        intent.putExtra("contact2Name",getcontact2Name);
        intent.putExtra("numero",getnumero);
        intent.putExtra("Role", getRole);
        startActivity(intent);
    }

}


