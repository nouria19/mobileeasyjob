package com.example.easyjob.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class Agence implements Parcelable {
    private Long id;
    private String Agencename;
    private String departementName;
    private String subDepartementName;
    private String companyNationalId;
    private String contact1Name;
    private String contact2Name;
    private int numero;


    private String Email;
    private String motDepasse;

    private List<Offre> offres = new ArrayList<Offre>();

    private UserRole role;

    public Agence(String email, String password, String agenceName, String departmentName,
                   String subDepartmentName, String companyNationalId, String contact1Name,
                   String contact2Name, int numero) {
        this.Email = email;
        this.motDepasse = password;
        this.Agencename = agenceName;
        this.departementName = departmentName;
        this.subDepartementName = subDepartmentName;
        this.companyNationalId = companyNationalId;
        this.contact1Name = contact1Name;
        this.contact2Name = contact2Name;
        this.numero = numero;
    }
    protected Agence(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        Agencename = in.readString();
        departementName = in.readString();
        subDepartementName = in.readString();
        companyNationalId = in.readString();
        contact1Name = in.readString();
        contact2Name = in.readString();
        numero = in.readInt();
        in.readTypedList(offres, Offre.CREATOR);
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getContact2Name() {
        return contact2Name;
    }

    public void setContact2Name(String contact2Name) {
        this.contact2Name = contact2Name;
    }

    public void setSubDepartementName(String subDepartementName) {
        this.subDepartementName = subDepartementName;
    }

    public void setContact1Name(String contact1Name) {
        this.contact1Name = contact1Name;
    }


    public void setCompanyNationalId(String companyNationalId) {
        this.companyNationalId = companyNationalId;
    }

    public String getCompanyNationalId() {
        return companyNationalId;
    }

    public void setDepartementName(String departementName) {
        this.departementName = departementName;
    }

    public void setName(String name) {
        this.Agencename = name;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getContact1Name() {
        return contact1Name;
    }

    public String getName() {
        return Agencename;
    }

    public String getSubDepartementName() {
        return subDepartementName;
    }

    public String getDepartementName() {
        return departementName;
    }


    public Long getId() {
        return id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeLong(id);
        }
        parcel.writeString(Agencename);
        parcel.writeString(departementName);
        parcel.writeString(subDepartementName);
        parcel.writeString(companyNationalId);
        parcel.writeString(contact1Name);
        parcel.writeString(contact2Name);
        parcel.writeInt(numero);
        parcel.writeTypedList(offres);
    }


    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMotDepasse() {
        return motDepasse;
    }

    public void setMotDepasse(String motDepasse) {
        this.motDepasse = motDepasse;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public List<Offre> getOffres() {
        return offres;
    }

    public void setOffres(List<Offre> offres) {
        this.offres = offres;
    }
}

