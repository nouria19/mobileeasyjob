package com.example.easyjob.Entity;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Candidat implements Parcelable  {
    private Long Id;
    private String nom;
    private String prenom;
    private String nationalite;
    private Date dateNaiss;
    private String Email;
    private String motDepasse;

    private UserRole role;

    public Candidat(String nom,String prenom,String nationalite,Date dateNaiss,String Email,String motDepasse) {
        this.nom = nom;
        this.prenom=prenom;
        this.nationalite=nationalite;
        this.dateNaiss=dateNaiss;
        this.Email=Email;
        this.motDepasse=motDepasse;
    }


    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        this.Id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public Date getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public String getMotDepasse() {
        return motDepasse;
    }

    public void setMotDepasse(String motDepasse) {
        this.motDepasse = motDepasse;
    }
    protected Candidat(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Candidat> CREATOR = new Creator<Candidat>() {
        @Override
        public Candidat createFromParcel(Parcel in) {
            return new Candidat(in);
        }

        @Override
        public Candidat[] newArray(int size) {
            return new Candidat[size];
        }
    };

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
